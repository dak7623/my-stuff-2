def bfs(g, startnode, numnodes):
    """
    Perform a breadth-first search on g starting at node startnode and
    exploring up to numnodes unique nodes.

    Returns a tuple containing the distances from startnode to each
    node, the predecessors of each node in the BFS search tree, the
    labels of each edge in the BFS search tree, and the set of visited
    nodes.
    """
    dist = {startnode:0}
    pred = {startnode:None}
    label = {}
    visited = set([startnode])
    tempkey = 0
    counter = 0
    # Fill in code
    # for i in range(len(g))
    # while len(visited) not equal len(g)
    for i in range(startnode, numnodes): #TODO: because the order of visited is not pre-determined
        for x in g[i]:   #thing is in graph set points
            if x not in visited:
                visited.add(x)
                pred[x] = i #if in both don't add again
                label[(i,x)] = 't'
            else:
                if (x,i) in label and label[(x,i)] == 't':
                    label[(x,i)] = 't'
                else:
                    label[(i,x)] = 'c'
                    if (x,i) in label and label[(x,i)] == 'c':
                        del label[(i,x)]

        # pred[key] = value
    for a in range(startnode+1, numnodes):
        tempkey = a
        counter = 0
        while tempkey != startnode:
            tempkey = pred[tempkey]
            counter+=1
        dist[a] = counter
    return (dist, pred, label, visited)


g1 = {
    0: set([1, 2]),
    1: set([3]),
    2: set([0, 3, 4]),
    3: set([1, 2, 5]),
    4: set([2, 5, 6]),
    5: set([3, 4]),
    6: set([4])
}

# visited: {0, 1, 2, 3, 4, 5, 6})
# pred: {0:none, 1:0, 2:0, 3:1, 4:2, 5:3, 6:4}
print(bfs(g1,0,7))
        