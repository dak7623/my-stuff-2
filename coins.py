import unittest
'''
Given an amount and the denominations of coins available, determine how many ways change can be made for amount. There is a limitless supply of each coin type.

Example
n = 3
c =[8, 3, 1, 2]

There are ways to make change for n=3: {1, 1, 1}, {1, 2}, and {3}.

n = 5
c = [1, 2, 3] -> [3, 2, 1]

Function Description

Complete the coinExchange function. getWays has the following parameter(s):
    int n: the amount to make change for
    int[] c: the available coin denominations

Returns
    int: the number of ways to make change
sum =      [0, 1, 2, 3, 4, 5]
c = [1] -> [0, 1, 1, 1, 1, 1]
c = [1, 2] [0, 1, 2/2+1, 3/2+1, 4/2+1, 5/2 + 1]
c=[1,2,3]  [0, 1, 2/3+2/2+1, 3/3+3/2+1, 4/3+4/2+1, 5/3+5/2+1]
c=[1,2,3,4][0, 1, 2/4+2/3+2/2+1, 3/4+3/3+3/2+1, 4/4+4/3+4/2+1, 5/4+5/3+5/2+1]

Basically, each time you add a number x, you add the number of formations to the column + x, and 1 for each time it is the target.
lookup 
'''
def coinExchange1(n, c):
    c = sorted(c)
    result = []
    sums = []
    if(len(c) == 0):
        return 1
    for i in range(n+1):
        sums.append(i)
    for i in sums:
        if(i%c[0] == 0 and i != 0):
            result.append(1)
        else:
            result.append(0)
    for j in c[1:]:
        for i in range(len(result)):
            if(result[i] != 0 and i + j < len(result)):    #If the possible number of ways to make a value is x, then the possible number
                result[i+j] += result[i]                   # of ways to make value+a where a is a new number is x + ways to form a
            if(sums[i] % j == 0 and i != 0):
                result[i] += 1   
    return result[-1]

def coinExchange(n, c):
    n_perms = [1]+[0]*n
    for coin in c:
        for i in range(coin, n+1):
            n_perms[i] += n_perms[i-coin]
    return n_perms[n]


class CoinExchange_Test(unittest.TestCase):
    # Base tests
    def test_1(self):
        self.assertEqual(coinExchange(4, [1, 2, 3]), 4)
        self.assertEqual(coinExchange(0, []), 1)
        self.assertEqual(coinExchange(1, [1]), 1)
        self.assertEqual(coinExchange(2, [1]), 1)
        self.assertEqual(coinExchange(3, [1]), 1)
        self.assertEqual(coinExchange(4, [1, 2]), 3)
    def test_2(self):
        self.assertEqual(coinExchange(10, [2, 5, 3, 6]), 5)
    def test_3(self):
        self.assertEqual(coinExchange(219, [36, 10, 42, 7, 50, 1, 49, 24, 37, 12, 34, 13, 39, 18, 8, 29, 19, 43, 5, 44, 28, 23, 35, 26]), 168312708)
        self.assertEqual(coinExchange(69, [25, 27, 40, 38, 17, 2, 28, 23, 9, 43, 18, 49, 15, 24, 19, 11, 1, 39, 32, 16, 35, 30, 48, 34, 20, 3, 6, 13, 44]), 101768)
        self.assertEqual(coinExchange(2, [44, 5, 9, 39, 6, 25, 3, 28, 16, 19, 4, 49, 40, 22, 2, 12, 45, 33, 23, 42, 34, 15, 46, 26, 13, 31, 8]), 1)
        self.assertEqual(coinExchange(245, [16, 30, 9, 17, 40, 13, 42, 5, 25, 49, 7, 23, 1, 44, 4, 11, 33, 12, 27, 2, 38, 24, 28, 32, 14, 50]), 64027917156)
if __name__ == '__main__':
    unittest.main()
    