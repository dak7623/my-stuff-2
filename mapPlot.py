import networkx as nx
import pylab as pl
import webbrowser

def draw_web_map(mGraph, region):
    """
    Draw a map of South America or the USA given a MapGraph that
    contains a dictionary of colors that maps two-letter country/state
    abbreviations to a color (represented as a number from 0 to n-1
    for an n-coloring).  Uncolored states will be white.
    """
    if not ((region == 'south_america') or (region == 'usa')):
        print("Error: 'region' argument must be 'south_america' or 'usa'.")
        return

    url = "http://chart.apis.google.com/chart?cht=t&chtm="
    url = url + region
    url = url + "&chs=440x220&chco=FFFFFF,FFFF00,00FF00,0000FF,FF0000"
    url = url + "&chld="

    colors = mGraph.get_colors()
    for node in colors.keys():
        url = url + node
    url = url + "&chd=t:"

    maxcolor = max(colors.values())
    scale = 100 / (maxcolor)

    first = True
    for node, color in colors.items():
        if not first:
            url = url + ","
        url = url + str(color * scale)
        first = False
    url = url + "&chf=bg,s,EAF7FE"
    webbrowser.open_new_tab(url)

def show_map_graph(mGraph):
    """
    Using networkx, draw a colored MapGraph that contains a dictionary
    of colors that maps two-letter country/state abbreviations to a
    color (represented as a number from 0 to n-1 for an n-coloring).
    Supports up to 12 colors.  Uncolored nodes will not be drawn.
    """
    fig = pl.figure()
    pl.title(mGraph.name)
    pos = nx.spring_layout(mGraph)
    c = ['#ffff00', '#00ff00', '#3030ff', '#ff0000', 
         '#00ffff', '#ff00ff', '#308070', '#703080', 
         '#807030', '#60a0d0', '#d060a0', '#a0d060']
    colors = mGraph.get_colors()
    maxcolor = max(colors.values())
    for i in range(maxcolor+1):
        nodes = []
        for node in mGraph.nodes():
            if colors[node] == i:
                nodes.append(node)
        nx.draw_networkx_nodes(mGraph, pos, 
                               nodelist=nodes, 
                               node_color=c[i])
    nx.draw_networkx_edges(mGraph,pos)
    nx.draw_networkx_labels(mGraph,pos)
    fig.show()