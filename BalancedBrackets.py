# Balanced Brackets

# Description
# A bracket is considered to be any one of the following characters: (, ), {, }, [, or ].
# Two brackets are considered to be a matched pair if the an opening bracket (i.e., (, [, or {) occurs to the left of a closing bracket (i.e., ), ], or }) of the exact same type. There are three types of matched pairs of brackets: [], {}, and ().
# A matching pair of brackets is not balanced if the set of brackets it encloses are not matched. For example, {[(])} is not balanced because the contents in between { and } are not balanced. The pair of square brackets encloses a single, unbalanced opening bracket, (, and the pair of parentheses encloses a single, unbalanced closing square bracket, ].
# By this logic, we say a sequence of brackets is balanced if the following conditions are met:
# - It contains no unmatched brackets.
# - The subset of brackets enclosed within the confines of a matched pair of brackets is also a matched pair of brackets.

# Given  a string of brackets, determine whether it is balanced. Return True if it is balanced, or False if not balanced.

# Example Inputs and Outputs
# Input         Output
# {[()]}        True
# {[(])}        False
# {{[[(())]]}}  True
# {(){{[]}}}(([]))
# [{((}

import unittest

def brackets(thing):
    ok_indices = []
    brace = False
    index = 0
    a = 0
    count = 0
    if len(thing) == 0:
        return True
    if len(thing)%2 == 1:
        return False
    while index < len(thing):
        if thing[index] == ")":
            a = index
            index -= 1
            if index == -1:
                return False
            if thing[index] != "(" and index not in ok_indices:
                if index == -1:
                    return False
                elif thing[index] == "{" or thing[index] == "[":
                    if index not in ok_indices:
                        return False
                
            ok_indices += [index]
            index = a
            count += 2
        elif thing[index] == "]":
            a = index
            index -= 1
            if index == -1:
                return False
            if thing[index] != "[" and index not in ok_indices:
                if index == -1:
                    return False
                elif thing[index] == "{" or thing[index] == "(":
                    if index not in ok_indices:
                        return False
                
            ok_indices += [index]
            index = a
            count += 2
        elif thing[index] == "}":
            a = index
            index -= 1
            if index == -1:
                return False
            if thing[index] != "{" and index not in ok_indices:
                if index == -1:
                    return False
                elif thing[index] == "(" or thing[index] == "[":
                    if index not in ok_indices:
                        return False
            ok_indices += [index]
            index = a
            count += 2
        index += 1
    return count == len(thing)
print(brackets("{()}"))

class MatchingBrackets_Test(unittest.TestCase):
    def test_matching_brackets(self):
        self.assertEqual(brackets("{[()]}"), True)
        self.assertEqual(brackets("{[(])}"), False)
        self.assertEqual(brackets("{{[[(())]]}}"), True)
        self.assertEqual(brackets("{()(){{[]}}}(([]))"), True)
        self.assertEqual(brackets("{{{{(()}}}}"), False)
        self.assertEqual(brackets("{}"), True)
        self.assertEqual(brackets("]["), False)

if __name__ == '__main__':
    unittest.main()

