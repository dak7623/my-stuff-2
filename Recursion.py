import unittest
'''
Recursion
A list is either:
	empty (base case), or
	Non-empty (inductive case), in which case it consists of
A data element, called “first”, and another list, called “rest”.
'''

# Part 1 - procedural sum
'''
[1, 2, 3, 4] -> 10

def sum_procedural(alist):
    result = 0
    for e in alist:
        result = result + e
    return result
'''
def sum_recursive(alist):
    if not alist: # if []
       return 0
    else:
       return alist[0]+sum_recursive(alist[1:]) # a = [1, 2, 3], a[1:]-> [2, 3]
'''
# Part 3 - text concatenation

Input

first_names = ['Alice','Bob','Carol','David']
last_names = ['Alligator','Bear','Chimpanzee','Deer']
Output

text_concat(first_names,last_names)
['Alice Alligator','Bob Bear','Carol Chimpanzee','David Deer']

first_names = ['Alice','Bob','Carol','David']
last_names = ['Alligator','Bear','Chimpanzee','Deer']
'''
def text_concat(alist, blist):
    if not alist:
        return blist
    if not blist:
        return alist
    return [alist[0] + " " + blist[0]] + text_concat(alist[1:], blist[1:])
'''
# Part 4 - double reverse

Input
name = ['Kyler', 'Larsen']

Output
double_reverse(name)
['nesraL', 'relyK']
double_reverse(alist)
'''
def double_reverse(alist):
    if not alist:
        return []
	# ['a', 'b', 'c'] => ['abc'] => ''.join(['a', 'b', 'c']) -> 'abc'
    return [alist[-1][-1] + ''.join(double_reverse(alist[-1][:-1]))] + double_reverse(alist[:-1])

'''
functional programming
[1, 2] -> [0, 1, 3]
'''
def partial_sum(alist):
    if not alist:
        return [0]
    return partial_sum(alist[:-1]) + [sum(alist)]

# Part 6 - partial sum
'''
Input
alist = [1,2,3,4,5]
Output
partial_sums(alist)
[0,1,3,6,10,15]
'''

def power_set(aset):
    if not aset:
        return[[]]
    else: 
        sub_set = power_set(aset[1:]) #+ power_set(aset[1:]) + power_set(aset[-1]) + 2# [1] + [2, 3] =? [1, 2, 3]
        sub_set2 = power_set(aset[1:])
        for s in sub_set:
            s.insert(0, aset[0])
        return sub_set2 + sub_set

def powerset(aset):
    '''
    Base case:
        [] -> []
    Recursive case:
        [1, 2] -> [[1]+]
    '''
    if not aset:
        return [aset]
    return [[aset[0]] + s for s in powerset(aset[1:])] + powerset(aset[1:]) # list comprehension

# Part 7 - power set
'''
Input 
aset = [0, 1, 2]
Output
powerset(aset)
[[], [0], [1], [2], [1, 0], [1, 2], [0, 2], [0, 1, 2]]
'''

# Part 8
'''
Input
l = [1, [2, [3, [[[]]]]]]
Output
6
'''
def sum_genlist(agenlist):
    if not agenlist:
        return 0
    else: 
        if isinstance(agenlist[0], list):
            return sum_genlist(agenlist[0]) + sum_genlist(agenlist[1:])
        else:
            return agenlist[0] + sum_genlist(agenlist[1:])
'''
[1, [2]]
sum_genlist([1, [2]])3
sum_genlist(1+ sum_genlist([2])) 3
sum_genlist(1 + 2)
'''
class Recursion(unittest.TestCase):
    def test_add_strlist_rec(self):
        self.assertEqual(text_concat(['Alice','Bob','Carol','David'], ['Alligator','Bear','Chimpanzee','Deer']), ['Alice Alligator','Bob Bear','Carol Chimpanzee','David Deer'])
        self.assertEqual(text_concat([], []), [])
        self.assertEqual(text_concat([], ['Alligator','Bear','Chimpanzee','Deer']), ['Alligator','Bear','Chimpanzee','Deer'])
        self.assertEqual(text_concat(['Alice','Bob','Carol','David'], []), ['Alice','Bob','Carol','David'])
        self.assertEqual(text_concat(['Alice','Bob'], ['Alligator','Bear','Chimpanzee','Deer']), ['Alice Alligator','Bob Bear','Chimpanzee','Deer'])
        self.assertEqual(text_concat(['Alice','Bob','Carol','David'], ['Alligator','Bear','Chimpanzee']), ['Alice Alligator','Bob Bear','Carol Chimpanzee','David'])
    def test_double_reverse(self):
        self.assertEqual(double_reverse([]), [])
        self.assertEqual(double_reverse(['abc']), ['cba'])
        self.assertEqual(double_reverse(['abc', 'def']), ['fed', 'cba'])
        self.assertEqual(double_reverse(['abcDEF', 'ghiJKL']), ['LKJihg', 'FEDcba'])
    def test_partial_sum(self):
        self.assertEqual(partial_sum([1,2,3]), [0,1,3,6]) # partial_sum([1,2]) + [6]
        self.assertEqual(partial_sum([1,2]), [0,1,3]) # partial_sum([1]) + [3]
        self.assertEqual(partial_sum([1]), [0,1])
        self.assertEqual(partial_sum([]), [0])
    def test_power_set(self):
        self.assertCountEqual(power_set([0,1,2]), [[],[0],[1],[0,1],[2],[1,2],[0,2],[0,1,2]])#[power_set([0,1]) + (power_set([0,1]) + [2]))]
        self.assertCountEqual(power_set([0,1]), [[],[0],[1],[0,1]]) #[power_set([0]) + [1] + [0,1]]
        self.assertCountEqual(power_set([0]), [[],[0]]) #[power_set([]) + [0]]
        self.assertCountEqual(power_set([]), [[]])
    def test_sum_genlist(self):
        self.assertEqual(sum_genlist([1,2,[3]]), 6)
        self.assertEqual(sum_genlist([[1],2,[3]]), 6)
        self.assertEqual(sum_genlist([1,[2,[3]]]), 6)
        self.assertEqual(sum_genlist([1,[],2, [3,[]],[4]]),10)
        self.assertEqual(sum_genlist([]), 0)


if __name__ == '__main__':
    unittest.main()

