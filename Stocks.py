import unittest
 
'''
You now know what the share price of GreenThumb (GT) will be for the next number of days. Each day, you can either buy one share of GT, sell any number of shares of GT that you own, or not make any transaction at all. What is the maximum profit you can obtain with an optimum trading strategy?
 
Example
prices = [1, 2]
Buy one share day one, and sell it day two for a profit of 1. Return 1.
 
prices = [2, 1]
No profit can be made so you do not buy or sell stock those days. Return 0.
 
Max_stock has the following parameter(s):
    prices: an array of integers that represent predicted daily stock prices
 
Returns
    int: the maximum profit achievable
 
Input Format
    Input contains n space-separated integers , each a predicted stock price for day .
 
Constraints
    predicted prices n: 1<=n<=50000
    prices: 1<=prices[i]<=100000
 
Output Format
    The maximum profit which can be obtained for the corresponding test case.
 
Example 1:
prices = [3, 2, 1] -> 0
 
Example 2:
prices = [1, 2, 500] -> 997
 
Example 3:
prices = [1, 2, 1, 2] -> 2
'''
def max_stock(prices):
    maxes = sorted(prices) 
    stock = 0
    profit = 0
    for i in range(len(prices)):
        if prices[i] == maxes[-1]:
            profit += stock*prices[i]
            maxes.remove(maxes[-1])
            stock = 0
        else:
            stock += 1
            profit -= prices[i]
            maxes.remove(prices[i])
    return profit

def max_stock_lan(stockPrices):
    n = len(stockPrices)
    optimalPrices = [0] * n
    optimalPrices[n - 1] = stockPrices[n - 1]
    for i in range(n - 2, -1, -1):
        optimalPrices[i] = max(stockPrices[i], optimalPrices[i + 1])

    maxProfit, shares = 0, 0
    for i in range(n):
        # Sell when next day's optimal price is less than today's optimal price
        if i == n - 1 or optimalPrices[i + 1] < optimalPrices[i]:
            # sell all shares
            maxProfit += shares * optimalPrices[i]
            shares = 0
        # Buy when stock price is less than optimal price for the same day
        elif stockPrices[i] < optimalPrices[i]:
            # buy one share
            shares += 1
            maxProfit -= stockPrices[i]

    return maxProfit
class Stock_Test(unittest.TestCase):
    # Base tests
    def test_stock_1(self):
        self.assertEqual(max_stock([1, 4, 1, 2]), 4)
        self.assertEqual(max_stock([1, 2, 1, 2]), 2)
        self.assertEqual(max_stock([3, 2, 1]), 0)
        self.assertEqual(max_stock([1, 2, 500]), 997)
    def test_stock_2(self):
        self.assertEqual(max_stock([1, 2, 3, 8, 3, 2, 1, 5]), 27)
        self.assertEqual(max_stock([1, 2, 3, 2, 4, 5, 8, 3, 1]), 31)
        self.assertEqual(max_stock([1, 2, 3, 2, 4, 5, 8, 3, 1, 9, 10]), 62)
        self.assertEqual(max_stock([40, 18, 17, 14, 16, 13, 30, 29, 25, 27, 21, 41]), 201)     
 
if __name__ == '__main__':
    unittest.main()
