import networkx as nx
import simplejson as json
import mapchecker 
import mapPlot 
class Stack: 
    """
    A stack class that can push/pop items on to/off of the top of the 
    stack and be cleard.

    LIFO - Stack
    FIFO - Queue
    """
    def __init__(self): 
        """ Create empty stack"""
        self.x = []
    def push(self, item): 
        """Push 'item' onto the top of the stack"""
        self.x.append(item)
    def pop(self): 
        """Remove and return the item on the top of the stack"""
        return self.x.pop()
    def clear(self): 
        """Clear stack so that it is empty"""
        self.x = []
    def is_empty(self): 
        """Return  True if stack is empty, False otherwise"""
        return (len(self.x) == 0)
    def __str__(self):
        return str(self.x)

class MapGraph(nx.Graph):
    """
    A graph class representing a map with colored nodes.
    """
    def __init__(self):
        """
        Create an empty MapGraph.
        """
        nx.Graph.__init__(self)
        self.colors = {}

    def add_node_and_edges(self, node, neighbors): # C, [D, B]
        """
        Add 'node' to MapGraph and edges from 'node' to the nodes in 'neighbors' that are already in the graph.

        self.add_node(C)
        self.add_edge(C, D)
        self.add_edge(C, B)
        """
        self.add_node(node)
        for i in neighbors:
            self.add_edge(node, i)

    def color_nodes(self):
        """
        Color the nodes of the graph using smallest-last coloring.

        1. Remove nodes from the graph to the stack
        2. Move nodes back into graph and color them
        """
        s = Stack()
        while len(self.degree) >= 1:
            # Find min degree in graph
            a=[]
            for node_name, degree in self.degree():
                a += [degree]
            
            # Remove node with min degree from graph and add it to the stack           
            for node_name, degree in self.degree():
                if degree == min(a):
                    s.push((node_name, list(self.neighbors(node_name))))
                    self.remove_node(node_name) #self.neighbors(n)
                    break
        '''
        take item off stack .pop
        check the items neighbors for colors
        choose which color the item can be (start at 0,1,2,3)
        add node_name and color to dictionary
        '''
        while s.is_empty() == False:
            node, neighbors = s.pop()
            color_neighbors = []
            self.add_node_and_edges(node, neighbors)
            self.colors[node] = 0
            for neighbor in neighbors:
                if neighbor in self.colors:
                    color_neighbors += [self.colors[neighbor]]
           '''
            isColor = true
            while isColor:
                isColor = false
                for neighbor in neighbors:
                   if neighbor in self.colors and self.colors[neighbor] == self.colors[node]:
                      self.colors[node] += 1
                       isColor = true
            '''
            while self.colors[node] in color_neighbors:
                self.colors[node] += 1
        return max(self.colors.values())+1
    '''
    self.neighbor('CO') ->  ['BR', 'VE']
    ('CO', ['BR', 'VE', 'PE'])
    [('CO', 'BR'), ('CO', 'EC'), ('CO', 'VE'), ('CO', 'PE'), ('BR', 'VE'), ('BR', 'BO'), ('BR', 'GY'), ('BR', 'GF'), ('BR', 'AR'), ('BR', 'PE'), ('BR', 'PY'), ('BR', 'UY'), ('BR', 'SR'), ('EC', 'PE'), ('VE', 'GY'), ('PE', 'CL'), ('PE', 'BO'), ('GY', 'SR'), ('CL', 'AR'), ('CL', 
'BO'), ('AR', 'BO'), ('AR', 'PY'), ('AR', 'UY'), ('BO', 'PY'), ('SR', 'GF')]
'''
    def get_colors(self):
        """
        Get dictionary of node -> color.
        """
        return self.colors.copy()

def load_json_file(filename):
    """
    Load object from 'filename'. The data contained within the file must be in JSON format.
    open()
    json.load()
    close()
    """
    j = open(filename)
    a = json.load(j)
    j.close()
    return a
    

def build_graph(filename):
    """
    Build and return a graph from a dictionary of adjacencies stored in JSON file named 'filename'.

    Use MapGraph
    """
    f=load_json_file(filename)
    g = MapGraph()
    for i in f:
        g.add_node_and_edges(i, f[i])
    return g

mapGraph = build_graph("automap.json")
print(mapGraph.color_nodes())
print(mapGraph.nodes)
print(mapGraph.edges)
print(mapchecker.verify_coloring(load_json_file('automap.json'), mapGraph.get_colors()))
#['CO', 'B', 'R', 'E', 'C', 'V', 'P', 'VE', 'O', 'G', 'Y', 'CL', 'A', 'BO', 'L', 'GY', 'S', 'GF', 'AR', 'U', 'EC', 'BR', 'F', 'UY', 'SR', 
#'PY', 'PE']