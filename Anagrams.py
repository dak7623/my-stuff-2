import unittest
'''
Two strings are anagrams of each other if the letters of one string can be rearranged to form the other string. Given a string, find the number of pairs of substrings of the string that are anagrams of each other.

Example

S = 'mom'

The list of all anagrammatic pairs is [m, m], [mo, om] at positions [[0], [2]], [[0,1], [1,2]] respectively.

Function Description
AnagramSubstrings has the following parameter(s):
    string s: a string

Returns
    int: the number of unordered anagrammatic pairs of substrings in s

Input Format
Contains a string S to analyze.

Constraints
2 <= len(S) <= 100
S contains only lowercase letters in the range ascii [a-z]

Example 1
Input: 'ifailuhkqq'
Output: 3 
Explanation: [i, i], [q, q], [ifa, fai]

Example 2
Input: 'kkkk'
Output: 10
Explanation: 6->[k, k], 3->[kk, kk], 1->[kkk, kkk]

Example 3
Input: 'cdcd'
Output: 5
Explanation: [cd, dc], [cd, cd], [dc, cd], [c, c], [d, d] 
'''

def divide_anagram(s):
    # [[], ['ab', 'cd', 'ba']]
    result = [[] for i in range(len(s)-1)]
    len_substring = 1
    while len_substring < len(s):
        end = 0
        while end + len_substring < len(s)+1:
            result[len_substring-1] += [''.join(sorted(s[end:end+len_substring]))]
            end += 1
        len_substring += 1
    return result
hi = divide_anagram("tacocat")
def AnagramSubstrings(s):
    counter = 0
    hi = divide_anagram(s)
    for i in hi:
        for a in range(len(i)):
            for b in i[a+1:]: # ['a', 'c', 'd', 'a', 'a']
                if b == i[a]:
                    counter += 1
    return counter
print(AnagramSubstrings("tacocat"))
'''
['a', 'b', 'c', 'd', 'e', 'f']
['ab', 'bc', 'cd', 'de', 'ef']
['abc', 'bcd', 'cde', 'def']  
['abcd', 'bcde', 'cdef']      
['abcde', 'bcdef']
['abcdef']

def anagram(s):
    len_substring = 1
    end_position = 0
    start_position = 0
    moving_start = 0
    moving_end = 0
    count = 0
    while len_substring < len(s):
        while moving_end < len(s):
            if equal_anagram(s[start_position:end_position+len_substring-1], s[moving_start:moving_end+len_substring-1]):
                count += 1
            moving_start += 1
            moving_end += 1
 '''   
class Anagrams_Test(unittest.TestCase):
    def test_matching_brackets(self):
        self.assertEqual(AnagramSubstrings('abba'), 4)
        self.assertEqual(AnagramSubstrings('abcd'), 0)
        self.assertEqual(AnagramSubstrings('aaaa'), 10)
        self.assertEqual(AnagramSubstrings('abab'), 5)
        self.assertEqual(AnagramSubstrings('ifailuhkqq'), 3)
        self.assertEqual(AnagramSubstrings('kkkk'), 10)   
        self.assertEqual(AnagramSubstrings('cdcd'), 5)
    
if __name__ == '__main__':
    unittest.main()