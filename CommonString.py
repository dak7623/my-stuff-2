import itertools
from re import L
import unittest
'''
A string is said to be a child of another string if it can be formed by deleting 0 or more characters from the other string. Letters cannot be rearranged. Given two strings of equal length, what's the longest string that can be constructed such that it is a child of both?

Example
s1 = 'ABCD'
s2 = 'ABDC'

These strings have two children with maximum length 3, ABC and ABD. They can be formed by eliminating either the D or C from both strings. Return .

Function Description

Complete the commonChild function. commonChild has the following parameter(s):
- string s1: string A
- string s2: string B

Returns
- int: the length of the longest string which is a common child of the input strings

Constraints
- All characters are upper case in the range ascii[A-Z].

Example 1:                             AYZCD
s1 = 'HARRY'                           ZAYDC
s2 = 'SALLY'
output: 2 ('AY')

 H A R R Y    
S- - - - -
A- A A A A
L- A A A A
L- A A A A
Y- A A A AY

 J E R R Y
M- - - - -
E- E E E E 
R- E ER ER ER
R- E ER ERR ERR
Y- E ER ERR ERRY+

 A B  C   D
AA A A    A
BA AB AB  AB
DA AB AB  ABD
CA AB ABC ABDC
'''
def commonChild(a, b):
    result = []
    if(len(a) == 0 or len(b) == 0):
        return 0
    for i in range(len(a)+1):
        result.append([])
        for j in range(len(b)+1):
            result[i].append(0)
    for i in range(len(a)):
        for j in range(len(b)):
            if a[i] == b[j]:
                result[i][j] = result[i-1][j-1] + 1
            else:
                if result[i][j-1] > result[i-1][j]:
                    result[i][j] = result[i][j-1]
                else:
                    result[i][j] = result[i-1][j]
    return result[-2][-2]
'''
def commonChild1(a, b):
    if a == b:
        return len(a)
    charA = []
    charB = []
    result = []
    for i in range(len(a)):
        charA.append(a[i])
    for i in range(len(b)):
        charB.append(b[i])
    i = 0
    while True:
        if charA[i] not in charB:
            charA.remove(charA[i])
        if charB[i] not in charA:
            charB.remove(charB[i])
        i += 1
        if(i > len(charA) - 1 or i > len(charB) - 1):
            break
    for i in range(len(a), 0, -1):
        subA = list(itertools.combinations(charA, i))
        subB = list(itertools.combinations(charB, i))
        for i in subA:
            if i in subB:
                return len(i)
    return 0
'''

class CoinExchange_Test(unittest.TestCase):
    # Base tests
    def test_1(self):
        self.assertEqual(commonChild("HARRY", "SALLY"), 2)
        self.assertEqual(commonChild("JERRY", "MERRY"), 4)
        self.assertEqual(commonChild("ABCD", "ABDC"), 3)
    def test_2(self):
        self.assertEqual(commonChild('TERRACED', 'CRATERED'), 5)
        self.assertEqual(commonChild("HALLOWEEN", "HALLWAY"), 5)
    def test_3(self):
        self.assertEqual(commonChild('ABC', 'D'), 0)
        self.assertEqual(commonChild('ABC', 'B'), 1)
        self.assertEqual(commonChild('A', ''), 0)
        self.assertEqual(commonChild('', 'B'), 0)
        self.assertEqual(commonChild('', ''), 0)
    def test_4(self):
        self.assertEqual(commonChild('ADXGSCDJYBSYU', 'GWHXDRNIDAXVU'), 3)
        self.assertEqual(commonChild('HZDATOUECKROUKJWPOFFZYGONIGPKMORYYLUBOIEOLOKPAQU', 'IFOWYXYEVOPZZP'), 7)
        self.assertEqual(commonChild('IFOWYXYEVOPP', 'HZDATOUECKROUKJWPOFFZYGONIGPKMORYYLUBOIEOLOKPAQU'), 7)
if __name__ == '__main__':
    unittest.main()   
