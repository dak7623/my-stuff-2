mapping = ["0", "1", "ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ"];

def mnemonicHelper(phoneNumber, digit, partialMnemonic, mnemonics):
    if digit == len(phoneNumber): # once the set of digits are completed, append to mnemonics
        mnemonics.append(''.join(partialMnemonic))
    else:
        for c in mapping[int(phoneNumber[digit])]: # looping through each char in the mapping
            partialMnemonic[digit] = c
            mnemonicHelper(phoneNumber, digit+1, partialMnemonic, mnemonics)

def mnemonic(phoneNumber):
    partialMnemonic = ['']*len(phoneNumber)
    mnemonics = []
    mnemonicHelper(phoneNumber, 0, partialMnemonic, mnemonics)
    return mnemonics