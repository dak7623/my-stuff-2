import unittest
import math
'''
t0 = 1000
j0 = 100
for days 1, 2, ... 30:
	td+1 = 2(td-jd)
	jd+1 = 2jd
def quitDay():
    td = 1000
    jd = 100
    counter = 1
    for days in range(30):
        td = 2*(td-jd)
        jd = 2*jd
        print(td, jd)
        counter += 1
        if td == jd:
            print(counter)
            break
def quitDay2():
    td = 1000
    jd = 100
    counter = 1
    total = 900
    for days in range(30):
        td = 2*(td-jd)
        jd = 2*jd
        total += td-jd
        print(td, jd, total)
        counter += 1
        if total <= 0:
            print(counter)
            break

def salary(start, stop):
    #td = start
    #inc = start
    for salary in range(start, stop):
        td = salary
        jd = 100
        #salary = inc
        for thing in range(30):
            td = 2*(td-jd)
            jd = 2*jd
        if td == 0:
            print("Tom's starting salary should be $", salary)
        #inc += 1

quitDay()
quitDay2()

salary(1,1000000000000000000)


Pseudocode - Binary Search
(start+stop)/2
higher is ((start+stop)/2 + stop)/2
lower is (start + (start+stop)/2)

while L <= R:
    M = (L+R)/2
    if M = T:
        return M
    else if M < T:
        L = M
    else:
        R = M
'''
def salary_binary_search(start, stop):
    while start <= stop:
        jd = 100
        middle = (start + stop)/2
        td = middle
        for thing in range(30):
            td = 2*(td-jd)
            jd = 2*jd
        if abs(td) < 1.0e5:
            if td > 3000:
                return math.floor(middle)
            if td < 3000:
                return math.ceil(middle)
        if td < 0:
            start = middle
        if td > 0:
            stop = middle
    return None
print(salary_binary_search(0,1000))
'''
class TomJerry_Test(unittest.TestCase):
    def test_halfInterval(self):
        self.assertEqual(salary_binary_search(2000, 4000), 3000)
        self.assertEqual(salary_binary_search(0, 10000), 3000)
        self.assertEqual(salary_binary_search(1000, 10000), 3000)
        self.assertEqual(salary_binary_search(1000, 5000), 3000)
        self.assertEqual(salary_binary_search(0, 1000), None)
        self.assertEqual(salary_binary_search(5000, 10000), None)
        self.assertEqual(salary_binary_search(0, 10014), 3000)
        self.assertEqual(salary_binary_search(0, 12205), 3000)
        self.assertEqual(salary_binary_search(-100, 12205), 3000)
        # self.assertEqual(salary_binary_search(-100, 3000), 3000)
        # self.assertEqual(salary_binary_search(-100, 3001), 3000)
        # self.assertEqual(salary_binary_search(0, 3000), 3000)
        # self.assertEqual(salary_binary_search(3000, 3000), 3000)
        
if __name__ == '__main__':
	unittest.main()
    '''