'''
Tic-Tac-Toe Project Description: 
Allow 2 players to play in the game of tic-tac-toe. EaEach play takes turn picking a square on the visualized board. 
Player 1 is represented by ('x'), player 2 is represented by ('o'). 
Once a player reaches 3 of its own markers in a horizontal, verticle, or diagonal row, the player wins the game.
If all possible locations on the board is filled and no side was able to complete a 3 in a row, the game ends in a tie.
  |   | 
----------
  | y | x
----------
  |   | 
  
'''
def tic_tac_toe_checker(board, x_name, y_name, turn):
    won = False
    for c in range(3):
        if board[c][0] == board[c][1] == board[c][2] == turn:
            won = True
            if turn == 'x':
                print(x_name, "won!")
            else:
                print(y_name, "won!")
        elif board[0][c] == board[1][c] == board[2][c] == turn:
            won = True
            if turn == 'x':
                print(x_name, "won!")
            else:
                print(y_name, "won!")
    if board[0][0] == board[1][1] == board[2][2] == turn or board[2][0] == board[1][1] == board[0][2] == turn:
        won = True
        if turn == 'x':
            print(x_name, "won!")
        else:
            print(y_name, "won!")
    return won
def check_side(board, turn):
    won = False
    for c in range(3):
        if board[c][0] == board[c][1] == board[c][2] == turn :
            won = True
        elif board[0][c] == board[1][c] == board[2][c] == turn:
            won = True
    if board[0][0] == board[1][1] == board[2][2] == turn or board[2][0] == board[1][1] == board[0][2] == turn:
        won = True
    return won
def make_move(board, turn, row, column):
    if board[row-1][column-1] == 'x' or board[row-1][column-1] == 'o':
        print("That space is already taken. Please choose again")
        success = False
    else:
        board[row-1][column-1] = turn
        success = True
    return success
def print_board(board): 
    for column_index in range(0, len(board)): 
        row = " "
        for row_index in range(0, len(board[column_index])): 
            if row_index != len(board[column_index]) -1 : 
                row += board[column_index][row_index] + " |"
            else: 
                row += board[column_index][row_index]
        print(row)
        if column_index !=len(board) -1: 
            print("----------")
def game_loop():
    print("Welcome to tic-tac-toe. x's go first")
    x_name = input("What is your name x? ")
    y_name = input("What is your name o? ")
    board = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]
    turn = 'x'
    c = 0
    while c < 9:
        success = False
        row = int(input("Row #"))
        column = int(input("Column #"))
        move = make_move(board, turn, row, column)
        win = check_side(board, turn)
        print_board(board)
        if move == True:
            c = c + 1
        if win == True:
            tic_tac_toe_checker(board, x_name, y_name, turn)
            break
        if (c % 2) == 1:
            turn = 'o'
        else:
            turn = 'x'
        if c == 9:
            print("Tie!")
again = "yes"
while again == "yes":       
    game_loop()
    again = input("Play again? (yes or no)")

