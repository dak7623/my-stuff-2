import math
import sys
sys.setrecursionlimit(1500)
def f(n):
    if int(math.sqrt(n) + 0.5)**2 == n:
        n = math.sqrt(n)
        return n
    else:
        return 1 + f(n+1)
def g(n):
    if int(math.sqrt(n)+0.5)**2 == n:
        n = math.sqrt(n)
        return n
    else:
        return 2 + g(n+2)
for i in range(1,999):
    if f(i)/g(i) == 4/7:
        print(f(i)/g(i))
        print(i)
        break