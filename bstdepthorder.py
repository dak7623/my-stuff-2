import binarytree
from collections import deque
import unittest
'''
BST_5
             19
         /        \
        7          43
      /   \        / \
      3   11      23  47
     / \   \      \    \
    2  5    17     37   53
            /     / \
           13    29  41
                  \
                  31 
result: [[19], [7, 43], [3, 11, 23, 47], [2, 5, 17, 37, 53], [13, 29, 41], [31]]
Use queue, no recursion
'''
def BinaryTreeDepthOrder(root):
    q = deque()
    q.append(root)
    result = []
    res = []
    qe = []
    while len(q) > 0:
        for rot in q:
            res += [rot.data]
            qe += [rot]
        q.clear()
        for rt in qe:
            if rt.left:
                q.append(rt.left)
            if rt.right:
                q.append(rt.right)
        result += [res]
        res = []
        qe = []
    return result
    

class BTInDepthOrder_Test(unittest.TestCase):
    def test_BTDO(self):
        self.assertEqual(BinaryTreeDepthOrder(binarytree.getBinaryTree(0)),
        [[2], [1, 3]])
        self.assertEqual(BinaryTreeDepthOrder(binarytree.getBinaryTree(1)),
        [[2], [1]])
        self.assertEqual(BinaryTreeDepthOrder(binarytree.getBinaryTree(2)),
        [[5], [4, 8], [11, 13, 4], [7, 2, 1]])
        self.assertEqual(BinaryTreeDepthOrder(binarytree.getBinaryTree(3)),
        [[4], [2, 5], [1, 3]])
        self.assertEqual(BinaryTreeDepthOrder(binarytree.getBinaryTree(4)),
        [[314], [6, 6], [2, 2], [3, 3]])
        self.assertEqual(BinaryTreeDepthOrder(binarytree.getBinaryTree(5)),
        [[19], [7, 43], [3, 11, 23, 47], [2, 5, 17, 37, 53], [13, 29, 41], [31]])

if __name__ == '__main__':
    unittest.main()