# Part 1 - Primes - primes are numbers that can only be divisible by 1 and itself. 
# 1 is not a prime number
import math
import unittest
def is_prime(n):
    for c in range(2,n):
        if n%c == 0:
            return False
    return True
# print(is_prime(3))
# print(is_prime(7))
# print(is_prime(97))

# get_prime: find all the prime numbers up to but not including m
def get_prime(m):
    primes = []
    for c in range(2,m):
        if is_prime(c):
            primes += [c]
    return primes
#print(get_prime(10))
#print(get_prime(100))

# filter: filter(predicate, list)
def get_prime_func(m): 
    return filter(is_prime, range(2, m))
print(get_prime_func(10))
print(get_prime_func(100))

def get_prime_func_2(m):
    def is_prime_inner(n):
        for k in range(2, n):
            if n%k == 0:
                return False
        return True
    return filter(is_prime_inner, range(2, m))

# filter all even numbers between 1-m
def get_even_func_2(m):
    def is_even_inner(n):
        return not (not n%2)
    return filter(is_even_inner, range(1,m))

# Part 4 - anonymous functions
# re-write get_even_func as an anynomous function, use the filter function to find all even numbers 
# between 1 and m. 
def get_even_func(m):
    return filter(lambda n: not (not n%2), range(1, m))

# Part 5 - anonymous functions
# rewrite is_prime using anonymous function
def is_prime_anonymous(m):
    return not list(filter(lambda n: not m%n, range(2,m))) #[] != True
class FuncProg(unittest.TestCase):
    def test_primes(self):
        self.assertEqual(list(get_even_func_2(2)), [1])
        self.assertEqual(list(get_even_func_2(3)), [1])
        self.assertEqual(list(get_even_func_2(5)), [1,3])
        self.assertEqual(is_prime_anonymous(17), True)
        self.assertEqual(is_prime_anonymous(88), False)
        self.assertEqual(is_prime_anonymous(83), True)
if __name__ == '__main__':
    unittest.main()