import unittest

'''
Subsequence: any subset of an array.
Subarray: a contiguous subsequence in an array.

Given an array, find the maximum possible sum among:
all nonempty subarrays.
all nonempty subsequences.
Note that empty subarrays/subsequences should not be considered.

Return maximum sum of subarray and maximum sum of subsequences.

Example 1: array = [-1, 2, 3, -4, 5, 10]
The maximum subarray sum is comprised of [2, 3, -4, 5, 10]. Their sum is 16. The maximum subsequence sum is comprised of [2, 3, 5, 10]  and their sum is 20.

Example 2: array = [1, 2, 3, 4]
The maximum subarray sum is comprised of [1, 2, 3, 4]. Their sum is 10. The maximum subsequence sum is comprised of [1, 2, 3, 4]  and their sum is 10.

Example 3: array = [2 -1 2 3 4 -5]
The maximum subarray sum is comprised of [2, -1, 2, 3, 4]. Their sum is 10. The maximum subsequence sum is comprised of [2, 2, 3, 4]  and their sum is 11.

Example 4: array = [-2 -3 -1 -4 -6]
The maximum subarray sum is comprised of [-1]. Their sum is -1. The maximum subsequence sum is comprised of [-1]  and their sum is -1.

'''
from cgi import print_form


def maxsequence(arr):
    sum = 0
    m = -1000000
    s = []
    all_neg = True
    for i in arr:
        if i >= 0:
            s += [i]
            all_neg = False
    if all_neg:
        for i in arr:
            m = max(m, i)
        s += [m]
    return s
def maxSubArray(a, sub):
    temp = 0
    sum = 0
    m = -1000000
    arr = []
    for i in range(len(a)+1):
        for j in range(i):
            sub += [a[j: i]]
    for i in sub:
        temp = 0
        for j in i:
            temp += j
        if temp > sum:
            sum = max(sum, temp)
            arr = []
            for n in i:
                arr += [n]
    if len(arr) == 0:
        for i in a:
            m = max(m, i)
        arr += [m]
    return arr
array =  [-2, -3, -1, -4, -6]
print("The maximum subarray sum is comprised of {0}. Their sum is {1}. The maximum subsequence sum is comprised of {2} and their sum is {3}.".format(maxSubArray(array, []), sum(maxSubArray(array, [])), maxsequence(array), sum(maxsequence(array))))

def maxSequence(a):
    return(sum(maxSubArray(a, [])), sum(maxsequence(a)))
class MaxSubSequenceArray_Test(unittest.TestCase):
    # Base tests
    # @unittest.skip
    def test_1(self):
        self.assertEqual(maxSequence([-1, 2, 3, -4, 5, 10]), (16, 20))
        self.assertEqual(maxSequence([1, 2, 3, 4]), (10, 10))
        self.assertEqual(maxSequence([2, -1, 2, 3, 4]), (10, 11))
        self.assertEqual(maxSequence([-2, -3, -1, -4, -6]), (-1, -1))
        self.assertEqual(maxSequence([-10]), (-10, -10))
        self.assertEqual(maxSequence([-1, -2, -3, -4, -5, -6]), (-1, -1))
        self.assertEqual(maxSequence([1, -1, -1, -1, -1, 5]), (5, 6))

if __name__ == '__main__':
    unittest.main()