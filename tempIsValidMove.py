def isValidMove(m):
    if(m.card.value != m.to.value-1):
        return False
    if(m.card.suit == "spades" or m.card.suit == "clubs"):
        if(m.to.suit != "hearts" and m.to.suit != "diamonds"):
            return False
    if(m.card.suit == "hearts" or m.card.suit == "diamonds"):
        if(m.to.suit != "spades" and m.to.suit != "clubs"):
            return False
    return True