class Car:
    def __init__(self, build, mpg):
        self.build = build
        self.mpg = mpg

class Car_Node:
    def __init__(self, data=None, next=None):
        self.data = data # Car
        self.next = next # Car_Node

    def __repr__(self):
        node = self
        visited = set()
        first = True
        result = ''
        # if node == None:
        #     return ''
        while node:
            if first:
                first = False
            else:
                result += '->'

            if id(node) in visited:
                if node.next is not node:
                    result += str(node.data.mpg)
                    result += '->...->'
                if node.data is not None:
                    result += str(node.data.mpg)
                    result += '->...'
                    break
            else:
                if node.data is not None:
                    result += str(node.data.mpg)
                    visited.add(id(node))
            node = node.next

        return result

'''
buildLinkedList - given a list of numbers, build a linked list, return head
a (head) -> b -> c -> None (tail)
[ car1, car2]
car1 -> car_node
node_car1.next -> node_car2
'''
def buildLinkedList_iter(dataSet):
    for i in range(len(dataSet)): # for i in range()
        if i+1 < len(dataSet):
            node = Car_Node(dataSet[i], dataSet[i+1])
            if i == 0:
                final = node
        else:
            node = Car_Node(dataSet[i], None)
    return final

def buildLinkedList(dataSet):
    # a if condition else b
    # return None if dataSet == [] else Car_Node(dataSet[0], buildLinkedList(dataSet[1:]))
    if dataSet == []:
        return None
    node = Car_Node(dataSet[0], buildLinkedList(dataSet[1:]))
    return node

node = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.0), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])

#print(node.__repr__())
