gameboard = [["_","_","_"], ["_","_","_"], ["_","_","_"]]
turn = 0
def print_board(gameboard):
    for j in range(3):
        print(gameboard[0][j] + " " + gameboard[1][j] + " " + gameboard[2][j])
def valid_place(gameboard, x, y):
    if(x < 1 or x > 3 or y < 1 or y > 3 or gameboard[x-1][y-1] != "_"):
        return False
    else:
        return True
def win_check(gameboard):
    for i in range(3):
        if(gameboard[i][0] == "X" and gameboard[i][1] == "X" and gameboard[i][2] == "X"):
            return True
        if(gameboard[i][0] == "O" and gameboard[i][1] == "O" and gameboard[i][2] == "O"):
            return True
        if(gameboard[0][i] == "X" and gameboard[1][i] == "X" and gameboard[2][i] == "X"):
            return True
        if(gameboard[0][i] == "O" and gameboard[1][i] == "O" and gameboard[2][i] == "O"):
            return True
    if(gameboard[0][0] == "X" and gameboard[1][1] == "X" and gameboard[2][2] == "X"):
        return True
    if(gameboard[0][0] == "O" and gameboard[1][1] == "O" and gameboard[2][2] == "O"):
        return True
    if(gameboard[0][2] == "X" and gameboard[1][1] == "X" and gameboard[2][0] == "X"):
        return True
    if(gameboard[0][2] == "O" and gameboard[1][1] == "O" and gameboard[2][0] == "O"):
        return True
    else:
        return False
while(not win_check(gameboard)):
    print_board(gameboard)
    print("Player", turn%2+1, "'s turn")
    st = input("Enter the x-coord and y-coord of the position seperated by a space (top left is 1 1)")
    x_coord, y_coord = st.split()
    x_coord = int(x_coord)
    y_coord = int(y_coord)
    if(valid_place(gameboard, x_coord, y_coord)):
        turn += 1
        if(turn%2 == 1):
            gameboard[x_coord-1][y_coord-1] = 'X'
        else:
            gameboard[x_coord-1][y_coord-1] = 'O'
    else:
        print("Space already taken!")
    if(turn == 9 and not win_check(gameboard)):
        print("Tie!")
        print_board(gameboard)
        break
if(win_check(gameboard)):
    print_board(gameboard)
    print("Player", turn%2, " won!")