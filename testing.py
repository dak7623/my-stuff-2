counter = 0
for i in range(10):
    if(i**2%10 == 0):
        counter = 0
    else:
        counter = 1
        a = i
        while(a//100 > 0 and a**2 % 10 == a**2 % 100):
            a = a//10
            counter += 1
        if counter > 1:
            print(i**2, " ", counter)