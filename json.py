import marko
import unittest
'''
Given a markdown list, serialize the list into a JSON object

Example:
Input:
- A
	- B
	- C
		- D
			- F
				- G
	- E

Output:
[
	{ A : [
		B,
		C : [
			D
		],
		E
	]}
]
marko.block.paragraph -> the actual letter
marko.block.list -> the depth of the letter and its children
marko.block.listitem -> contains either or both of the paragraph and list. If it contains only the paragraph, it has no children.
A has children B, C, E
Shown by A's children at positions 0, 1, 2 or marko.block.List's children after arriving at A.
C has child D
'''
def json_converter(text):
    p = marko.parse(text)
    if isinstance(p.children[0], marko.block.Paragraph):
        
    return p

def markdown_reader(file):
    text = ''
    with open(file) as f:
        text = f.read()
        f.close()
    return text
    
def init():
    text = ''
    with open('sample1.md') as f:
        text = f.read()
        p = marko.parse(text)
        nodes = p.children
        json_output = json_converter(text)
        print(json_output)
        f.close()

def json_converter(string):
    text = ''
    visited_list = []
    with open(string) as f:
        text = f.read()
        p = marko.parse(text)
        nodes = p.children
        tree_explorer(nodes, visited_list)
        f.close()
    return visited_list

def tree_explorer(children_node, visited):
    # Handle key/value
    if isinstance(children_node, list) and len(children_node) == 2:
        if isinstance(children_node[0], marko.block.Paragraph) and isinstance(children_node[1], marko.block.List):
            key = extract_paragraph_text(children_node[0])
            visited.append({key:[]})
            return tree_explorer(children_node[1], visited[-1][key])
    
    if isinstance(children_node, list) and len(children_node) == 1:
        children_node = children_node[0]

    if isinstance(children_node, marko.block.List):
        children_node = children_node.children
        for list_item in children_node:
            if len(list_item.children) == 1 and isinstance(list_item.children[0], marko.block.Paragraph):
                visited.append(extract_paragraph_text(list_item.children))
            else:
                tree_explorer(list_item.children, visited)
    # Leaf node
    elif isinstance(children_node, marko.block.Paragraph):
        text = extract_paragraph_text(children_node)
        visited.append(text)
        return

def extract_paragraph_text(children_node):
    while not isinstance(children_node, marko.inline.RawText):
        if isinstance(children_node, list):
            children_node = children_node[0]
        else:
            children_node = children_node.children
    return children_node.children

# init()
class JSON_Test(unittest.TestCase):
    def test_json_converter(self):
            self.assertEqual(json_converter(markdown_reader('sample1.md')), 
            [{'A':['B', {'C':['D']}, 'E']}])
            # self.assertEqual(json_converter('String/sample2.md'), 
            # [{'A':['B', {'C':[ {'D':['E']}]}]}])
            # self.assertEqual(json_converter('String/sample3.md'), 
            # [{'A':['B']}, {'C':['D']}, 'E'])
            # self.assertEqual(json_converter('String/sample4.md'), 
            # [{'A':['B', {'C':['D', 'E']}, 'F', 'G']}, {'H':['I', 'J', {'K':['L', 'M', 'N']}]}])

if __name__ == '__main__':
    unittest.main()