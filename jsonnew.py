import marko
import unittest
'''
Given a markdown list, serialize the list into a JSON object

Example:
Input:
- A
	- B
	- C
		- D
			- F
				- G
	- E

Output:
[
	{ A : [
		B,
		C : [
			D
		],
		E
	]}
]
marko.block.paragraph -> the actual letter
marko.block.list -> the depth of the letter and its children
marko.block.listitem -> contains either or both of the paragraph and list. If it contains only the paragraph, it has no children.
A has children B, C, E
Shown by A's children at positions 0, 1, 2 or marko.block.List's children after arriving at A.
C has child D
'''
def json_converter(p):
    
    kids = []
    while not isinstance(p, str) and not isinstance(p.children[0], marko.inline.RawText):
        json_converter(p.children[0])
    kids.append(p.children[0].children)
    return kids
    
    #if isinstance(p.children[0], marko.block.List):
    #    for i in p.children[0]:
    #        kids.appends(i)
    

def markdown_reader(file):
    p = None
    text = ''
    with open(file) as f:
        text = f.read()
        p = marko.parse(text)
        f.close()
    return p
    
def init():
    text = ''
    with open('sample1.md') as f:
        text = f.read()
        p = marko.parse(text)
        nodes = p.children
        json_output = json_converter(nodes)
        print(json_output)
        f.close()
# init()
class JSON_Test(unittest.TestCase):
    def test_json_converter(self):
            self.assertEqual(json_converter(markdown_reader('sample1.md')), 
            [{'A':['B', {'C':['D']}, 'E']}])
            # self.assertEqual(json_converter('String/sample2.md'), 
            # [{'A':['B', {'C':[ {'D':['E']}]}]}])
            # self.assertEqual(json_converter('String/sample3.md'), 
            # [{'A':['B']}, {'C':['D']}, 'E'])
            # self.assertEqual(json_converter('String/sample4.md'), 
            # [{'A':['B', {'C':['D', 'E']}, 'F', 'G']}, {'H':['I', 'J', {'K':['L', 'M', 'N']}]}])

if __name__ == '__main__':
    unittest.main()