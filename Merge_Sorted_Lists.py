from Car_Node import Car_Node
from Car_Node import buildLinkedList
from Car_Node import Car
import unittest

def delete_kth_to_last(head, k):
    dummyhead = Car_Node()
    a = tail = dummyhead
    dummyhead.next = head
    for i in range(k):
        tail = tail.next
    while tail.next:
        a = a.next
        tail = tail.next
    a.next = a.next.next
    return dummyhead.next
        


def delete_from_linked_list(head, value): #value -> car obj
    tail = head 
    yes = True
    if tail.data.build == value.build:
        tail = tail.next
        return tail
    while yes:
        if tail.next.data.build == value.build:
            tail.next = tail.next.next
            yes = False
        tail = tail.next
    return head

def insert_linked_list(head, value): #head -> sorted list
    tail = head
    yes = True
    if not head:
        return Car_Node(value, None)
    while yes:
        if not tail.next and value.mpg > tail.data.mpg:
            tail.next = Car_Node(value, None)
            yes = False
        elif value.mpg < tail.data.mpg:
            tail = Car_Node(value, tail)
            return tail
        elif tail.data.mpg < value.mpg and tail.next.data.mpg > value.mpg:
            tail.next = Car_Node(value, tail.next)
            yes = False
        tail = tail.next
    return head
# 1 -> 2 -> (3) -> 4

def has_cycle(head):
    a = tail = head
    counter = 0
    while a.next and a.next.next and tail.next:
        a = a.next.next
        tail = tail.next
        if a.data.mpg == tail.data.mpg:
            tail = tail.next
            counter += 1
            while a.data.mpg != tail.data.mpg:
                tail = tail.next
                counter += 1
            follow = head
            for i in range(counter):
                head = head.next
            while follow.data.mpg != head.data.mpg:
                follow = follow.next
                head = head.next
            return head
    return None

    '''
    visited = []
    while tail.next and tail.next.data.mpg not in visited:
        visited += [tail.data.mpg]
        tail = tail.next
    if not tail.next:
        return False
    else:
        return True
    visited += tail.next.data.mpg
    index = visited.index(visited[-1])
    for i in range(index+1):
    '''

def reverse_linked_list(head):
    # Base case
    if head is None or head.next is None:
        return head
    # None <- a <- first -> second -> None
    # Iteration
    first = second = head
    second = second.next
    first.next = None
    while second.next:
        temp = second.next
        second.next = first
        first = second
        second = temp
    second.next = first
    return second
def reverse_linked_list_half(head):
    pointer1 = pointer2 = head
    if head is None or head.next is None:
        return head
    while pointer2.next:
        if pointer2.next.next:
            pointer1 = pointer1.next
            pointer2 = pointer2.next.next
        else: 
            pointer1 = pointer1.next
            pointer2 = pointer2.next
    return reverse_linked_list(pointer1)
def linked_list_palindrom(head):
    a1 = a2 = head
    a2 = reverse_linked_list_half(a2)
    while a1.next:
        if a1.data.mpg != a2.data.mpg:
            return False
        a1 = a1.next
        a2 = a2.next
    return True

def singly_linked_to_doubly_linked(head):
    # TODO - you fill in here.
    return None

def merge_two_sorted_lists(l1, l2):
    # ()->15.8->
    head = tail = Car_Node()
    while l1 and l2:
        if l1.data.mpg < l2.data.mpg:
            tail.next = l1
            l1 = l1.next
        else:
            tail.next = l2
            l2 = l2.next
        tail = tail.next
    if l1 == None:
            tail.next = l2
    if l2 == None:
            tail.next = l1
    
    # handle case when one of the two lists runs out
    return head.next

def merge_sorted_lists(l_sorted):
    if len(l_sorted) == 0:
        return Car_Node()
    if len(l_sorted) == 1:
        return l_sorted[0]
    l = merge_two_sorted_lists(l_sorted[0], l_sorted[1])
    for i in range(len(l_sorted)-2):
        l = merge_two_sorted_lists(l, l_sorted[i+2])
    return l

def has_cycle_wrapper(head, cycle_idx):
    cycle_length = 0
    if cycle_idx != -1:
        if head is None:
            raise RuntimeError('Can\'t cycle empty list')
        cycle_start = None
        cursor = head
        while cursor.next is not None:
            if cursor.data.mpg == cycle_idx:
                cycle_start = cursor
            cursor = cursor.next
            cycle_length += 1 if cycle_start is not None else 0

        if cursor.data.mpg == cycle_idx:
            cycle_start = cursor
        if cycle_start is None:
            raise RuntimeError('Can\'t find a cycle start')
        cursor.next = cycle_start
        cycle_length += 1

class MergeSortedLists_Test(unittest.TestCase):
    # def test_has_cycle_1(self):
    #     self.assertEqual(has_cycle(buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])), 
    #     False)

    #     linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
    #     has_cycle_wrapper(linked_list_1, 22.8)
    #     self.assertEqual(has_cycle(linked_list_1), True)
    
    def test_reverse_linked_list(self):
        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
        self.assertEqual(reverse_linked_list(linked_list_1).__repr__(), "30.4->22.8->21.3->21.0->15.8")
    
    
    def test_linked_list_palindrom(self):
        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])        
        self.assertEqual(linked_list_palindrom(linked_list_1), False)

        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 21.0), Car("Lotus Europa", 15.8)])
        self.assertEqual(linked_list_palindrom(linked_list_1), True)

        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.0), Car("Datsun 710", 15.8)])
        self.assertEqual(linked_list_palindrom(linked_list_1), True)
        '''
    def test_has_cycle(self):
        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
        self.assertEqual(has_cycle(linked_list_1), None)

        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
        has_cycle_wrapper(linked_list_1, 22.8)
        self.assertEqual(has_cycle(linked_list_1).data.mpg, 22.8)

        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
        has_cycle_wrapper(linked_list_1, 21.0)
        self.assertEqual(has_cycle(linked_list_1).data.mpg, 21.0)

        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
        has_cycle_wrapper(linked_list_1, 22.8)
        self.assertEqual(has_cycle(linked_list_1).data.mpg, 22.8)
        
        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
        has_cycle_wrapper(linked_list_1, 15.8)
        self.assertEqual(has_cycle(linked_list_1).data.mpg, 15.8)

        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 81.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 53.8), Car("Lotus Europa", 30.4)])
        has_cycle_wrapper(linked_list_1, 21.0)
        self.assertEqual(has_cycle(linked_list_1).data.mpg, 21.0)
    def test_insert_linked_list(self):
        self.assertEqual(insert_linked_list(
            buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.0), Car("Lotus Europa", 30.4)]), Car("Datsun 710", 22.8)).__repr__(), "15.8->21.0->21.0->22.8->30.4")
        self.assertEqual(insert_linked_list(
            buildLinkedList([Car("Mazda RX4", 21.0)]), Car("Ford Pantera L", 15.8)).__repr__(), "15.8->21.0")
        self.assertEqual(insert_linked_list(
            buildLinkedList([Car("Ford Pantera L", 15.8)]), Car("Mazda RX4", 21.0)).__repr__(), "15.8->21.0")
        self.assertEqual(insert_linked_list(
            buildLinkedList([]), Car("Mazda RX4", 21.0)).__repr__(), "21.0")
    def test_delete_from_linked_list(self):
        self.assertEqual(delete_from_linked_list(
            buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.0), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)]), Car("Datsun 710", 22.8)).__repr__(), "15.8->21.0->21.0->30.4")
        self.assertEqual(delete_from_linked_list(
            buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0)]), Car("Ford Pantera L", 15.8)).__repr__(), "21.0")
    def test_mergesortedList(self):
        self.assertEqual(merge_two_sorted_lists(
            buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)]),
            buildLinkedList([Car("Volvo 142E", 21.4), Car("Toyota Corolla", 33.9)])
            ).__repr__(),
            "15.8->21.0->21.3->21.4->22.8->30.4->33.9")
        self.assertEqual(merge_sorted_lists(
            [
                buildLinkedList([Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8)]), buildLinkedList([Car("Hornet Sportabout", 18.1), Car("Valiant", 18.7)])
            ]).__repr__(), 
            "18.1->18.7->21.0->21.3->22.8")
        self.assertEqual(merge_sorted_lists(
            [
                buildLinkedList([]),
                buildLinkedList([Car("Hornet Sportabout", 18.1), Car("Valiant", 18.7)])
            ]).__repr__(),
            "18.1->18.7")
        self.assertEqual(merge_sorted_lists(
            [
                buildLinkedList([Car("Hornet Sportabout", 18.1), Car("Valiant", 18.7)]),
                buildLinkedList([])
            ]).__repr__(),
            "18.1->18.7")
        self.assertEqual(merge_sorted_lists(
            [
                buildLinkedList([Car("Hornet Sportabout", 18.1), Car("Valiant", 18.7)]),
            ]).__repr__(),
            "18.1->18.7")
        self.assertEqual(merge_sorted_lists(
           []).__repr__(),
           "")
        self.assertEqual(merge_sorted_lists(
            [
                buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.0), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)]), 
                buildLinkedList([Car("Hornet Sportabout", 18.1), Car("Valiant", 18.7), Car("Porsche 914-2", 26.0)]),
                buildLinkedList([Car("Volvo 142E", 21.4), Car("Toyota Corolla", 33.9)])
            ]).__repr__(), 
            "15.8->18.1->18.7->21.0->21.0->21.4->22.8->26.0->30.4->33.9")
    def test_delete_kth_to_last(self):
        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
        self.assertEqual(delete_kth_to_last(linked_list_1, 2).__repr__(), "15.8->21.0->21.3->30.4")
        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
        self.assertEqual(delete_kth_to_last(linked_list_1, 3).__repr__(), "15.8->21.0->22.8->30.4")
        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
        self.assertEqual(delete_kth_to_last(linked_list_1, 1).__repr__(), "15.8->21.0->21.3->22.8")
        linked_list_1 = buildLinkedList([Car("Ford Pantera L", 15.8), Car("Mazda RX4", 21.0), Car("Mazda RX4 Wag", 21.3), Car("Datsun 710", 22.8), Car("Lotus Europa", 30.4)])
        self.assertEqual(delete_kth_to_last(linked_list_1, 5).__repr__(), "21.0->21.3->22.8->30.4")
        '''
if __name__ == '__main__':
    unittest.main()
