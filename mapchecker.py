def verify_coloring(adjacencies, colormap):
    """
    Given a list of adjacencies and a map of nodes to colors, verify
    that all nodes have a color and that no two adjacent nodes have
    the same color.  Also checks that no colors between 0 and the
    maximum color number have been skipped and that all nodes in the
    color map are also in the adjacency list.
    
    Returns True if the coloring is valid, False otherwise.

    Example use: 
    mapchecker.verify_coloring(load_json_file('map.json'), mGraph.get_colors())
    """

    # Check that adjacent nodes have different colors
    for node, neighbors in adjacencies.items():
        for n in neighbors:
            if node not in colormap:
                print("Invalid coloring,", node, "has no color.")
                return False
            if n not in colormap:
                print("Invalid coloring,", n, "has no color.")
                return False
            if colormap[node] == colormap[n]:
                print("Invalid coloring, both", node, "and", n, "have the same color:", colormap[node])
                return False

    # Check that all nodes in the colormap are also in the adjacency list
    for node in colormap.keys():
        if node not in adjacencies:
            print("Invalid coloring,", node, "has a color but is not in the adjacency list.")
            return False

    # Check that no colors are skipped
    colors = colormap.values()
    for i in range(max(colors)):
        if i not in colors:
            print("Invalid coloring, skipped color:", i)
            return False

    # Passed all tests
    return True