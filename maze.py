import unittest
def enum(**enums):
    return type('Enum', (), enums)

#Color = enum(BLACK=0, WHITE=1)
WHITE, BLACK = range(2)

#Linked List
class Coordinate:
    def __init__(self, x, y, p):
        self.x = x
        self.y = y
        self.parent = p #Coordinate

# Return the path from start to end
def searchMaze(maze, start, end):
    frontier = [start]
    visited = []
    finish = (end.x,end.y)
    # figure where next possible paths are
    # choosing who to visit next
    # maze[start.x][start.y] 

    #currentCoord = frontier.pop()
    # Looking for newCoord's neighbors
    while len(frontier) != 0:
        currentCoord = frontier.pop()
        visited.append((currentCoord.x, currentCoord.y))
        # if validCoord(Coordinate(currentCoord.x+1, currentCoord.y, currentCoord)) and Coordinate(currentCoord.x+1, currentCoord.y, currentCoord) not in visited
        # if validCoord(currentCoord):
        if validCoord(Coordinate(currentCoord.x+1, currentCoord.y, currentCoord), maze) and maze[currentCoord.x+1][currentCoord.y] == WHITE and (currentCoord.x+1, currentCoord.y) not in visited:
            frontier.append(Coordinate(currentCoord.x+1, currentCoord.y, currentCoord))
        if validCoord(Coordinate(currentCoord.x-1, currentCoord.y, currentCoord), maze) and maze[currentCoord.x-1][currentCoord.y] == WHITE and (currentCoord.x-1, currentCoord.y) not in visited:
            frontier.append(Coordinate(currentCoord.x-1, currentCoord.y, currentCoord))
        if validCoord(Coordinate(currentCoord.x, currentCoord.y+1, currentCoord), maze) and maze[currentCoord.x][currentCoord.y+1] == WHITE and (currentCoord.x, currentCoord.y+1) not in visited:
            frontier.append(Coordinate(currentCoord.x, currentCoord.y+1, currentCoord))
        if validCoord(Coordinate(currentCoord.x, currentCoord.y-1, currentCoord), maze) and maze[currentCoord.x][currentCoord.y-1] == WHITE and (currentCoord.x, currentCoord.y-1) not in visited: 
            frontier.append(Coordinate(currentCoord.x, currentCoord.y-1, currentCoord))
            
        #maze[currendCoord.x][currentCoord.y] = BLACK
        if (currentCoord.x, currentCoord.y) == finish:
            break
        
        
    # trace path
    path = tracePath(currentCoord, start)
    # never reach ones it has previously visited
    # [1.1, 1.2, 1.3, 1.3.1, 1.3.2, ]
    #validCoord(Coordinate(currentCoord.x+1, currentCoord.y, currentCoord), maze)
    return path
'''
def findNeighbors(maze, node):
    # return a list of all possible neighbors of node
    positions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
    return filter(lambda n:isFeasible(maze, n), map(lambda x:Coordinate(node.x+x[0], node.y+x[1], None), positions))

def isFeasible(maze, node):
    # return boolean indicating whether or not the neighboring node can be processed
    col = len(maze[0])
    row = len(maze)
    return node.x >= 0 and node.x < row and node.y >= 0 and node.y < col and maze[node.x][node.y] == WHITE
'''
# validCoord - (checks whether the coord is within the bounds of the maze) and (that is it an empty space that can be explored)
def validCoord(coord, maze):
    # TODO: verify definition of what is a valid coordinate
    validCoord = False
    if coord.x < len(maze) and coord.x > -1 and coord.y < len(maze[coord.x]) and coord.y > -1:
        validCoord = True
    return validCoord

def tracePath(node, start):
    path = []
    while node.parent != None:
        #path.append((node.x, node.y))
        path.insert(0, (node.x, node.y))
        node = node.parent # Linked List
    path.insert(0, (node.x,node.y))
    return path
def searchmaze_wrapper(maze, start, end):
    path = searchMaze(maze, start, end)
    verify = False
    # verify
    for c in range(len(path)):
        if maze[path[c][0]][path[c][1]] == WHITE:
            verify = True
        else:
            verify = False
            break
    if path[0] != (start.x, start.y) or path[-1] != (end.x, end.y):
        verify = False
    return verify

#[(0, 1), (0, 0), (1, 0), (2, 0)]   
#maze = [[WHITE, WHITE, Color.WHITE], [Color.WHITE, Color.BLACK, Color.WHITE], [Color.WHITE, Color.BLACK, Color.WHITE]]
# w w w
# w b w
# w b w
maze = [[1, 0, 0, 0, 0, 0], [1, 1, 0, 1, 1, 0], [0, 1, 0, 1, 1, 0], [0, 0, 0, 0, 0, 0], [1, 1, 0, 0, 1, 0], [0, 1, 0, 0, 1, 0]]
print(searchMaze(maze, Coordinate(2, 0, None), Coordinate(5, 5, None)))

class Maze(unittest.TestCase):
    def test_maze(self):
        #maze = [[Color.WHITE, Color.WHITE, Color.WHITE], [Color.WHITE, Color.BLACK, Color.WHITE], [Color.WHITE, Color.BLACK, Color.WHITE]]
        maze = [[0, 0, 0], [0, 1, 0], [0, 1, 0]]
        self.assertEqual(searchmaze_wrapper(maze, Coordinate(2, 0, None), Coordinate(0, 1, None)), True)

        maze = [[1, 0, 0, 0, 0, 0], [1, 1, 0, 1, 1, 0], [0, 1, 0, 1, 1, 0], [0, 0, 0, 0, 0, 0], [1, 1, 0, 0, 1, 0], [0, 1, 0, 0, 1, 0]]
        self.assertEqual(searchmaze_wrapper(maze, Coordinate(2, 0, None), Coordinate(5, 5, None)), True)
        self.assertEqual(searchmaze_wrapper(maze, Coordinate(5, 0, None), Coordinate(5, 5, None)), False)
        self.assertEqual(searchmaze_wrapper(maze, Coordinate(0, 1, None), Coordinate(5, 0, None)), False)
        self.assertEqual(searchmaze_wrapper(maze, Coordinate(0, 1, None), Coordinate(4, 5, None)), True)

        maze = [[0, 1, 0, 1, 0], [0, 0, 0, 1, 0], [0, 1, 1, 0, 1], [1, 0, 0, 0, 0], [1, 0, 0, 1, 1], [0, 0, 0, 0, 0], [1, 0, 0, 1, 0], [0, 0, 0, 0, 0], [1, 0, 1, 0, 0], [0, 1, 0, 0, 0], [0, 0, 0, 1, 1], [0, 0, 0, 1, 0], [0, 0, 0, 1, 0], [1, 0, 0, 0, 0], [0, 1, 0, 0, 0], [0, 0, 0, 1, 0], [1, 0, 0, 1, 0], [1, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [1, 1, 1, 0, 0], [0, 0, 0, 0, 1], [0, 0, 0, 0, 1], [0, 0, 0, 0, 0], [0, 0, 0, 1, 1]]
        self.assertEqual(searchmaze_wrapper(maze, Coordinate(8, 3, None), Coordinate(17, 1, None)), True)

        maze = [[0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0], [0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], [0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1], [1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0], [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0], [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1], [0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0], [0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0], [0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1], [0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1], [1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0], [0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0], [0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1], [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0], [1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1], [0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0], [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1]]
        self.assertEqual(searchmaze_wrapper(maze, Coordinate(25, 2, None), Coordinate(19, 0, None)), True)

        maze = [[0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0], [1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0], [0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1], [0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1], [0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1], [0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1], [1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0], [0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0], [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1], [1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0], [0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1], [1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0], [1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0], [1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0], [1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0], [0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0], [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0], [0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0], [1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1]]
        self.assertEqual(searchmaze_wrapper(maze, Coordinate(21, 2, None), Coordinate(26, 16, None)), True)
if __name__ == '__main__':
    unittest.main()