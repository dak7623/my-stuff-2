import unittest
import binarytree
class Stack: 
    def __init__(self): 
        self.x = []
    def push(self, item): 
        self.x.append(item)
    def pop(self): 
        return self.x.pop()
    def clear(self): 
        self.x = []
    def is_empty(self): 
        return (len(self.x) == 0)
    def get_list(self):
        return self.x
    def peak(self):
        return self.x[-1]
    def __str__(self):
        return str(self.x)
    def len(self):
        return len(self.x)
'''
BST_5
             19
         /        \
        7          43
       / \         / \
      3   11      23 47
     / \   \      \    \
    2   5  17     37   53
            /     / \
           13    29  41
                  \
                  31 
root = 37 -> [29, 31, 37, 41]


'''

def BSTinSortedOrder(root):
    #root.left(), root.right(), root.data()
    s = Stack()
    final = []
    while not s.is_empty() or root != None:
        if root:
            s.push(root)
            root = root.left
        else:
            root = s.pop()
            final += [root.data]
            root = root.right
            '''
            root = s.peak()
            if root.right:
                root = root.right
            #s.push(root)
    '''

    return final
def BSTrecursive(root,alist):
    if root == None:
        return []
    else:
        BSTrecursive(root.left, alist)
        alist += [root]
        BSTrecursive(root.right, alist)


print(BSTinSortedOrder(binarytree.getBinaryTree(5)))