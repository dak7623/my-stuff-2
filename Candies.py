import unittest
'''
Alice is a kindergarten teacher. She wants to give some candies to the children in her class.  All the children sit in a line and each of them has a rating score according to his or her performance in the class. Alice wants to give at least 1 candy to each child. If two children sit next to each other, then the one with the higher rating must get more candies. Alice wants to minimize the total number of candies she must buy.

For example: 
arr = [4, 6, 4, 5, 6, 2]
She gives the students candy in the following minimal amounts: [1, 2, 1, 2, 3, 1] She must buy a minimum of 10 candies.

Function Description

Complete the candies function in the editor below.

candies has the following parameter(s):

int n: the number of children in the class
int arr[n]: the ratings of each student
Returns

int: the minimum number of candies Alice must buy
Input Format

The first line contains an integer, n, the size of arr.
Each of the next n lines contains an integer arr[i] indicating the rating of the student at position i.

Constraints
1 < n < 10^5
1 < arr[i] < 10^5

Example 1:
input: 1 2 2
output: 4 [1, 2, 1]

Example 2:
input: 2 4 2 6 1 7 8 9 2 1
output: 19 [1, 2, 1, 2, 1, 2, 3, 4, 2, 1]

Example 3:
input: 2 4 3 5 2 6 4 5
output: 12 [1, 2, 1, 2, 1, 2, 1, 2]
'''
def StudentFavoritism(rank):
    base = True
    for i in range(len(rank)-1):
        if rank[0] != rank[i+1]:
            base = False
            break
    if base == True:
        return len(rank)
    candy = [1]
    current_candy = 1
    adjust = 0
    longest_string = []
    final = []
    yes = False
    for i in range(len(rank)-1):
        if rank[i+1] > rank[i]:
            current_candy += 1
        else:
            current_candy = 1

        candy += [current_candy]
    while not yes:
        for i in range(len(candy)-1):
            if candy[i] == candy[i+1]:
                if rank[i] == rank[i+1] and i+2 < len(candy) and rank[i+1] != rank[i+2]:
                    candy[i+1] += 1
                else:
                    candy[i] += 1
        for i in range(len(rank)-1):
            if candy[i] == candy[i+1]:
                yes = False
                break
            else:
                yes = True
    for i in range(len(rank)):
        if i+1 < len(rank) and rank[i] == rank[i+1]:
            longest_string += [i]
            
        else:
            longest_string += [i]
            if len(longest_string) > adjust:
                adjust = len(longest_string)
                final = []
                for i in longest_string:
                    final += [i]
            longest_string = []
    for i in final:
        candy[i] = 1
    if final[0] > 0 and rank[final[0]] > rank[final[0]-1]:
        candy[final[0]] = candy[final[0]-1] + 1
    if final[-1]+1 < len(rank) and rank[final[-1]] > rank[final[-1] + 1]:
        candy[final[-1]] = candy[final[-1]+1] + 1
        
    return sum(candy)

def Candies(n):
    a = n
    N = len(a)
    left, right, num = [[0 for i in range(N)] for j in range(3)]
    
    # Check left
    for p in range(1, N):
        if a[p] > a[p-1]:
            left[p] = left[p-1] + 1
    
    # Check right
    for p in range(N-2, -1, -1):
        if a[p] > a[p+1]:
            right[p] = right[p+1] + 1
    
    # Adjust distribution
    for p in range(N):
        num[p] = 1 + max(left[p], right[p])
    print(num)
    return sum(num)

class Candies_Test(unittest.TestCase):
    def test_matching_brackets(self):
    
        self.assertEqual(StudentFavoritism([1, 2, 2]), 4)
        self.assertEqual(StudentFavoritism([2, 4, 2, 6, 1, 7, 8, 9, 2, 1]), 19)
        self.assertEqual(StudentFavoritism([2, 4, 3, 5, 2, 6, 4, 5]), 12)
        self.assertEqual(StudentFavoritism([8, 2, 4, 3, 5, 2, 6, 4, 5]), 14)
        self.assertEqual(StudentFavoritism([10, 2, 4, 2, 6, 1, 7, 8, 9, 2, 1]), 21)
        self.assertEqual(StudentFavoritism([1, 1, 1]), 3)
        self.assertEqual(StudentFavoritism([1, 2, 3, 4, 5, 6, 7, 8]), 36)
        self.assertEqual(StudentFavoritism([8, 7, 6, 5, 4, 3, 2, 1]), 36)
        self.assertEqual(StudentFavoritism([8, 7, 8, 8, 4, 2, 3, 1]), 14)
        self.assertEqual(StudentFavoritism([8, 7, 8, 2, 4, 5, 3, 1]), 14)
        self.assertEqual(StudentFavoritism([8, 7, 8, 8, 8, 4, 2, 3, 1]), 15)
        self.assertEqual(StudentFavoritism([8, 7, 6, 5, 4, 3, 2, 1, 2, 3, 4, 5, 6, 7, 8, 7, 6, 5, 4, 3, 2, 1]), 99)
        self.assertEqual(StudentFavoritism([1, 2, 3, 4, 5, 6, 7, 8, 7, 6, 5, 4, 3, 2, 1, 2, 3, 4, 5, 6, 7, 8, 7, 6, 5, 4, 3, 2, 1]), 127)
        self.assertEqual(StudentFavoritism([8, 7, 8, 8, 8, 8, 4, 2, 3, 1]), 16) # [2, 1, 2, 1, 1, 3, 2, 1, 2, 1]
        self.assertEqual(StudentFavoritism([8, 7, 8, 8, 8, 8, 4]), 10)
        self.assertEqual(StudentFavoritism([8, 7, 8, 8, 8, 8, 8]), 9)
        self.assertEqual(StudentFavoritism([8, 7, 8, 8, 8, 8, 4, 2, 3, 1, 8, 8, 8, 8, 2]), 23)

if __name__ == '__main__':
    unittest.main()