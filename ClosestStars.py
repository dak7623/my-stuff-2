from typing import Iterator, List, Tuple
import math
import unittest
import heapq
class Star:
    def __init__(self, x: float, y: float, z: float) -> None:
        self.x, self.y, self.z = x, y, z

    @property
    def distance(self) -> float:
        return math.sqrt(self.x**2 + self.y**2 + self.z**2)

    def __lt__(self, rhs: 'Star') -> bool:
        return self.distance < rhs.distance

    def __repr__(self):
        return str(self.distance)

    def __str__(self):
        return self.__repr__()

    def __eq__(self, rhs):
        return math.isclose(self.distance, rhs.distance)
'''
def find_closest_k_stars(stars: Iterator[Star], k: int) -> List[Star]:
    star = []
    dist = []
    result = []
    for i in stars:
        s = Star(i[0], i[1], i[2])
        star.append(s)
    for i in star:
        dist.append(i.distance)
    star.sort(key = lambda a: a.distance)       
    return star[:k]
'''
def find_closest_k_stars(stars: Iterator[Star], k: int) -> List[Star]:
    star = []
    result = []
    for i in stars:
        s = Star(i[0], i[1], i[2])
        star.append(s)
    heapq.heapify(star)
    for i in range(k):
        result.append(heapq.heappop(star))
    return result
print(find_closest_k_stars([[0, 0, 3], [0, 0, 4], [0, 0, 5], [0, 0, 6], [0, 0, 1], [0, 0, 2]], 3))

class IClosestStars(unittest.TestCase):
    # Base tests
    @unittest.skip
    def test_0(self):
        self.assertEquals(find_closest_k_stars([[0, 0, 3], [0, 0, 4], [0, 0, 5], [0, 0, 6], [0, 0, 1], [0, 0, 2]], 3), )
if __name__ == '__main__':
    unittest.main()