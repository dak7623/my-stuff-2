import csv
import sys
import unittest
class TexasNutrition(object):
    def __init__(self):
        super().__init__()
        self._data = []
        self._header = []
        self._filePath = "2019_MealReimbursement.txt"
        self.importFile()
        self.schoolDictionary = self.schooltotal()
        self.countyDictionary = self.countytotal()
    def importFile(self):
        with open(self._filePath, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            self._header = next(reader)
            for row in reader:
                self._data.append(row)
    #use reduce : https://docs.python.org/3/library/functools.html#functools.reduce
    def totalFreeMeals(self):
        total = 0
        for c in self._data:
                total += int(c[self._header.index("BreakfastServedFree")])
                total += int(c[self._header.index("LunchServedFree")])
        return total
    def totalReimbursedMeals(self):
        total = 0
        for c in self._data:
                total += float(c[self._header.index("BreakfastReimbursement")])
                total += float(c[self._header.index("LunchReimbursement")])
        return total
    def totalReimburse(self):
        total = self.totalReimbursedMeals()
        for c in self._data:
                #LL - can call totalReimbursedMeals for breakfast + lunch reimbursements
                total += float(c[self._header.index("SnackReimbursement")])
                total += float(c[self._header.index("MilkReimbursement")])
        return total
    def avgMeals(self):
        return self.totalFreeMeals()/len(self._data)
    # dictionary[key] = value
    #LL - can combine school and county total together
    def dictionaryMap(self, keys):
        dictionary = {}
        
        for c in self._data:
            uniqueName = ""
            for a in keys:
                uniqueName += c[self._header.index(a)]
            if uniqueName in dictionary:
                dictionary[uniqueName] += float(c[self._header.index("BreakfastServedFree")]) + float(c[self._header.index("LunchServedFree")])
            else:
                dictionary[uniqueName] = float(c[self._header.index("BreakfastServedFree")]) + float(c[self._header.index("LunchServedFree")])
        return dictionary
    #LL - use dictionaryMap instead
    def schooltotal(self):
        return self.dictionaryMap(["SiteName", "CEID", "SiteID"])
    #same thing here
    def countytotal(self):
        return self.dictionaryMap(["SiteCounty"])
    def RatingDictionary(self, dictionary, isMax, numRange):
        items = []
        for x in range(numRange):
            # a if condition else b
            if isMax:
                temp = -1000000
            else:
                temp = 1000000
            key = ""
            for c in dictionary:
                if isMax and (dictionary[c] > temp):
                    temp = dictionary[c]
                    key = c
                elif not isMax and (dictionary[c] < temp):
                    temp = dictionary[c]
                    key = c
            items += [key]
            print([key])
            del dictionary[key] 
        return items
    def sortDictionary(self, dictionary):
        #return selectionSort(list(dictionary.items()))
        #return self.mergeSort(list(dictionary.items()))
        return sorted(list(dictionary.items()), key = lambda x:x[1], reverse = True)

    #sorted(iterable, *, key=None, reverse=False)
    #key=lambda x:x[1]

    

    def selectionSort(self, dictionaryList):
        for a in range(len(dictionaryList)-1):
            temp = -100000000
            key = ""
            for c in range(a, len(dictionaryList)):
                if dictionaryList[c][1] > temp:
                    temp = dictionaryList[c][1]
                    key = c
            dictionaryList[a], dictionaryList[key] = dictionaryList[key], dictionaryList[a]
        return dictionaryList

    def mergeSort(self, dictionaryList):
        # split the list
        # sort the left - sortDictionary
        # sort the right - sortDictionary
        # merge the two - merge 
        dictionaryLeftSorted = []
        dictionaryRightSorted = []
        if len(dictionaryList) == 1:
            return dictionaryList
        if len(dictionaryList) > 1:
            dictionaryLeft = dictionaryList[:len(dictionaryList)//2]
            dictionaryLeftSorted = self.mergeSort(dictionaryLeft)
            dictionaryRight = dictionaryList[len(dictionaryList)//2:]
            dictionaryRightSorted = self.mergeSort(dictionaryRight)
        return self.merge(dictionaryLeftSorted, dictionaryRightSorted)
    def merge(self, left, right):
        #compare first in left with first in right
        #add one to index of the side that is bigger
        mergeList = []
        a=0
        b=0
        for c in range(len(left) + len(right)):
            if b >= len(right) or (a < len(left) and left[a][1] > right[b][1]):
                mergeList += [left[a]]
                a += 1
            elif b < len(right):
                mergeList += [right[b]]
                b += 1
        return mergeList
            
    def report(self):
        print("1) Total number of free meals (breakfast and lunch) served across Texas during 2018-2019 school year: {0}".format(self.totalFreeMeals()))
        print("2) Total amount of meal (breakfast and lunch) reimbursement paid during 2018-2019 school year: {0}".format(self.totalReimbursedMeals()))
        print("3) Total amount of total reimbursement paid during 2018-2019 school year: {0}".format(self.totalReimburse()))
        print("4) Average monthly free meals provided during 2018-2019 school year: {0}".format(self.avgMeals()))
        print("5) Top 10 schools with the greatest number of free meals provided {0}".format(self.RatingDictionary(self.schoolDictionary, True, 10)))
        print("6) Top 10 schools with the least number of free meals provided{0}".format(self.RatingDictionary(self.schoolDictionary, False, 10)))
        print("7) Top 10 counties with the greatest number of freel meals provided{0}".format(self.RatingDictionary(self.countyDictionary, True, 10)))        
        print("8) Top 10 counties with the least number of free meals provided{0}".format(self.RatingDictionary(self.countyDictionary, False, 10)))

#s = TexasNutrition()
#s.report()
class FuncProg(unittest.TestCase):
    def test_primes(self):
        a = TexasNutrition()
        testDictionary = {"A":0, "B":1}
        self.assertEqual(a.RatingDictionary({"A":0, "B":1}, True, 1), ["B"])
        self.assertEqual(a.RatingDictionary({"A":0, "B":1}, True, 2), ["B","A"])
        self.assertEqual(a.RatingDictionary({"A":0, "B":1}, False, 1), ["A"])
        self.assertEqual(a.RatingDictionary({"A":0, "B":1}, False, 2), ["A","B"])
        #self.assertEqual(a.RatingDictionary({"A":0, "B":1, "C":0}, False, 2), ["A","B"])
    '''
    def test_sortDictionary(self):
        a = TexasNutrition()
        self.assertEqual(a.sortDictionary({"A":0, "B":1}), [("B", 1), ("A", 0)])
        self.assertEqual(a.sortDictionary({"C":0, "Y":26, "H":142}), [("H", 142), ("Y", 26), ("C", 0)])
        self.assertEqual(a.sortDictionary({}), [])  
        
    def test_merge(self):
        a = TexasNutrition()
        self.assertEqual(a.merge([5,3,1], [6,4,2]), [6,5,4,3,2,1])  
        self.assertEqual(a.merge([6,5,4], [3,2,1]), [6,5,4,3,2,1])   
        '''
    def test_sortDictionary(self):
        a = TexasNutrition()   
        self.assertEqual(a.sortDictionary({"A":0, "B":1}), [("B", 1), ("A", 0)])
        self.assertEqual(a.sortDictionary({"C":0, "Y":26, "H":142}), [("H", 142), ("Y", 26), ("C", 0)])
        self.assertEqual(a.sortDictionary({}), [])  
#if __name__ == '__main__':
#   unittest.main()