from tkinter import *
import enum 
class gameboard(object):
    def __init__(self):
        self.length = 8
        self.width = 8
        self.board=[["0", "1", "2", "3", "4", "5", "6", "7", "8"],
                    ["1", "E", "R", "E", "R", "E", "R", "E", "R"], 
                    ["2", "R", "E", "R", "E", "R", "E", "R", "E"], 
                    ["3", "E", "R", "E", "R", "E", "R", "E", "R"],
                    ["4", "E", "E", "E", "E", "E", "E", "E", "E"],
                    ["5", "E", "E", "E", "E", "E", "E", "E", "E"],
                    ["6", "B", "E", "B", "E", "B", "E", "B", "E"],
                    ["7", "E", "B", "E", "B", "E", "B", "E", "B"],
                    ["8", "B", "E", "B", "E", "B", "E", "B", "E"]]
    def __str__(self):
        result = ""
        for row in self.board:
            result += str(row) + "\n"
        return result    
board = gameboard()
print(board)
