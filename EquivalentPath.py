import unittest
class Stack: 
    def __init__(self): 
        self.x = []
    def push(self, item): 
        self.x.append(item)
    def pop(self): 
        return self.x.pop()
    def clear(self): 
        self.x = []
    def is_empty(self): 
        return (len(self.x) == 0)
    def get_list(self):
        return self.x
    def peak(self):
        return self.x[-1]
    def __str__(self):
        return str(self.x)
    def len(self):
        return len(self.x)
# {({ ()})} "{((})}" -> ["{", "("]

def shortest_equivalent_path(path):
    # TODO - you fill in here.
    '''
	"usr/lib/../bin/gcc" -> [usr,  lib, .., bin, gcc]
	level = 0 (-1, -2, -1, -2, -3)
	"/script/../script/./.." -> [script, .., script, ..]
    level = 0
    level > 0 [script, .., .., script, .., .., place] "script/../../something/../../place"
    ../../place

	[i for i in alist if i != "."]
	"/".join(alist)
    '''
    newpath = [i for i in path.split("/") if i != "." and i != ""] #/. -> [""]
    level = 0
    for string in newpath:
        if string == "..":
            level += 1
        else:
            level -= 1
    if level == 0:
        return '/'
    if level > 0:
        end = ""
        for i in range(level):
            end += "../"
        if not newpath[-1] == "..":
            end += "../"
            end += newpath[-1]
        else:
            end = end[:-1]
        return end # end[:-1]
    if level < 0:
        new = []
        for item in newpath:
            new += [item]
        for item in new:
                if item == "..":
                    if newpath[newpath.index(item)-1] and not newpath[newpath.index(item)-1] == "..":
                        del(newpath[newpath.index(item)-1])
                        del(newpath[newpath.index(item)])
        end = "/".join(newpath)
        return end        
def shortest_equivalent_path_stack(path):
    newpath = [i for i in path.split("/") if i != "." and i != ""]
    s = Stack()
    for item in newpath:
        if item == ".." and s.len() > 0 and s.peak() != "..":
            s.pop()
        else:
            s.push(item)
    if s.is_empty():
        return "/"
    else:
        return "/".join(s.get_list())


class EquivalentPaths_Test(unittest.TestCase):
	def test_shortest_equivalent(self):
		self.assertEqual(shortest_equivalent_path_stack("/"), "/") #absolute path, relative path - 
		self.assertEqual(shortest_equivalent_path_stack("/."), "/") # current directory
		self.assertEqual(shortest_equivalent_path_stack(".."), "..") # previous directory
		self.assertEqual(shortest_equivalent_path_stack("../"), "..")
		self.assertEqual(shortest_equivalent_path_stack("usr/lib/../bin/gcc"), "usr/bin/gcc")
		self.assertEqual(shortest_equivalent_path_stack("./.././../local"), "../../local")
		self.assertEqual(shortest_equivalent_path_stack("/script/../script/./.."), "/")
if __name__ == '__main__':
	unittest.main()