import unittest
class NewNode:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def left(self):
        return self.left

    def right(self):
        return self.right

    def setLeft(self, left):
        self.left = left

    def setRight(self, right):
        self.right = right

    def data(self):
        return self.data

'''
insert - inserting new node with given number into binary tree. 

newnode(2), 3
newnode(2)-newnode(3)
        4
       / \ 
      2   5 
     / \
    1   3

        4
       / \ 
      3   5 
     / 
    1   
'''
def insert(node, data):
    # Insert code here
    if node == None:
        return NewNode(data)
    return node

'''
isBST - checks if BT is a binary search tree 
'''
def isBST_v1(node):
    if node.left == None and node.right == None:
        return True
    else:
        if node.left and node.left.data <= node.data:
            return isBST(node.left)
        if node.right and node.right.data >= node.data: 
            return isBST(node.right)
        else:
            return False

def isBST_v2(node):
    node_list = []
    inorder_recursive(node, node_list)
    return node_list == sorted(node_list)

def isBST(node):
    #if find the max and the min of both sides of the tree
    # find max and min on left side, make sure that they are less than curr node
    # find max and min on right side, make sure that they are larger than curr node
    #   return isBST(node.left) and isBST(node.right)
    # return false
    if node == None:
        return True
    l = True
    r = True
    if (node.left != None):
        l = (maxVal(node.left)).data <= node.data and (minVal(node.left)).data <= node.data
    if (node.right != None):
        r = (maxVal(node.right)).data >= node.data and (minVal(node.right)).data >= node.data
    if (l and r):
        return isBST(node.left) and isBST(node.right)  
    else:
        return False
    
    if (node.left != None and maxVal(node.left) <= node and minVal(node.left) <= node) and (node.right != None and maxVal(node.right) >= node and minVal(node.right) >= node):
        return isBST(node.left) and isBST(node.right)
    
'''
def isBST(node): # wrapper function
    return(isBSTRecur(node, -sys.maxint, sys.maxint))

def isBSTRecur(node, int_min, int_max):
    if node == None:
        return True

    if node.data < int_min or node.data > int_max:
        return False
        
    return (isBSTRecur(node.left, int_min, node.data-1) and isBSTRecur(node.right, node.data+1, int_max))


hasPathSum - if root-to-leaf paths contains sum, return true, else return false
'''
def hasPathSum(node, summ):
    if node != None and node.data == summ:
        return True
    if node == None:
        return False
    return hasPathSum(node.left, summ - node.data) or hasPathSum(node.right, summ - node.data)


'''
hasPathSum - if root-to-leaf paths contains sum, return true, else return false
'''
def hasPathSum(node, sum):
    if node == None:
        return False
    if node.left == None and node.right == None:
        if (node.data == sum):
            return True
        else: 
            return False
    else:
        return (hasPathSum(node.left, sum-node.data) or hasPathSum(node.right, sum-node.data))

def maxVal(node):
    while node.right != None:
        node = node.right
    return node
def minVal(node):
    while node.left != None:
        node = node.left
    return node


#find the max and the min of both sides of the tree
#if all the elements of the tree are within these bounds then might be a tree
def inorder(node, list):
    nodes = []
    starts = []
    while node.left:
        starts += node
        node = node.left
    return nodes

def inorder_recursive(node, list):
    if node == None:
        return 
    inorder_recursive(node.left, list)
    list.append(node.data)
    inorder_recursive(node.right, list)
    '''
    pre-order
    list.append(node.data)
    inorder_recursive(node.left, list)
    inorder_recursive(node.right, list)
    post-order
    inorder_recursive(node.left, list)
    inorder_recursive(node.right, list)
    list.append(node.data)
    
    '''

'''
lookup - searches through binary tree to find target. If target exist, return true, else false.

divide and conquer
'''
def lookup(node, target):
    # Insert code here
    # Base case
        # no node, return false
    # Recursive
    if node == None:
        return False
    if node.data == target:
        return True
    return lookup(node.right, target) or lookup(node.left, target)

    #return lookup(node.left, target) or lookup(node.right, target)

'''
getBinaryTree - helper function that returns the binary tree
Bt_0 
    2 
   / \ 
  1   3

Bt_1 
    2
   /
  1

Bt_2
        5
       / \ 
      4   8 
     /    /\
    11   13 4
    /\       \
   7  2       1
Bt_3
        4
       / \ 
      2   5           
     / \ 
    1   3
pre order traversal (middle, left, right) - [4, 2, 1, 3, 5]
in order traversal (left, middle, right) - [1,2,3,4,5]
post order traversal (left, right, middle) - [1,3,2,5,4]
Bt_4 
     314
    /   \
   6     6
    \    /
     2  2
     \  /
      3 3
BST_5
             19
         /        \
        7          43
      /   \        / \
      3   11      23  47
     / \   \      \    \
    2  5    17     37   53
            /     / \
           13    29  41
                  \
                  31 
Bt_6
        4
       / \ 
      2   5           
     / \ 
    1   6

Bt_7
        4
       / \ 
      2   5           
     / \ 
    1   6
   / \
  0   3

Bt_8
        4
       / \ 
      2   5           
     / \   \
    1   6   7
           / \
          3   8

Bt_9
         4
       /  \ 
      2    5
     / \    \
    1   6    7
       /    / \
      3    3   8
     /
    2 
'''
def getBinaryTree(tree_number):
    bt = None
    if tree_number == 0:
        bt = NewNode(2)
        bt.setLeft(NewNode(1))
        bt.setRight(NewNode(3))
    elif tree_number == 1:
        bt = NewNode(2)
        bt.setLeft(NewNode(1))
    elif tree_number == 2:
        bt_11 = NewNode(11)
        bt_11.setLeft(NewNode(7))
        bt_11.setRight(NewNode(2))
        bt_4 = NewNode(4)
        bt_4.setLeft(bt_11)
        bt_4_2 = NewNode(4)
        bt_4_2.setRight(NewNode(1))
        bt_8 = NewNode(8)
        bt_8.setLeft(NewNode(13))
        bt_8.setRight(bt_4_2)
        bt = NewNode(5)
        bt.setLeft(bt_4)
        bt.setRight(bt_8)
    elif tree_number == 3:
        bt_2 = NewNode(2)
        bt_2.setLeft(NewNode(1))
        bt_2.setRight(NewNode(3))
        bt = NewNode(4)
        bt.setLeft(bt_2)
        bt.setRight(NewNode(5))
    elif tree_number == 4:
        bt_3 = NewNode(3)
        bt_2left = NewNode(2)
        bt_2left.setRight(bt_3)
        bt_6left = NewNode(6)
        bt_6left.setRight(bt_2left)
        bt_2right = NewNode(2)
        bt_2right.setLeft(bt_3)
        bt_6right = NewNode(6)
        bt_6right.setLeft(bt_2right)
        bt = NewNode(314)
        bt.setRight(bt_6right)
        bt.setLeft(bt_6left)
    elif tree_number == 5:
        bt_13 = NewNode(13)
        bt_2 = NewNode(2)
        bt_5 = NewNode(5)
        bt_17 = NewNode(17)
        bt_3 = NewNode(3)
        bt_11 = NewNode(11)
        bt_7 = NewNode(7)
        bt = NewNode(19)
        bt_43 = NewNode(43)
        bt_23 = NewNode(23)
        bt_47 = NewNode(47)
        bt_37 = NewNode(37)
        bt_53 = NewNode(53)
        bt_29 = NewNode(29)
        bt_41 = NewNode(41)
        bt_31 = NewNode(31)
        bt_17.setLeft(bt_13)
        bt_3.setLeft(bt_2)
        bt_3.setRight(bt_5)
        bt_11.setRight(bt_17)
        bt_7.setLeft(bt_3)
        bt_7.setRight(bt_11)
        bt_29.setRight(bt_31)
        bt_37.setLeft(bt_29)
        bt_37.setRight(bt_41)
        bt_23.setRight(bt_37)
        bt_47.setRight(bt_53)
        bt_43.setLeft(bt_23)
        bt_43.setRight(bt_47)
        bt.setLeft(bt_7)
        bt.setRight(bt_43)
    elif tree_number == 6:
        bt_2 = NewNode(2)
        bt_2.setLeft(NewNode(1))
        bt_2.setRight(NewNode(6))
        bt = NewNode(4)
        bt.setLeft(bt_2)
        bt.setRight(NewNode(5))
    return bt


class BinaryTree(unittest.TestCase):
    def test_lookup(self):
        self.assertEqual(lookup(getBinaryTree(0), 2), True)
        self.assertEqual(lookup(getBinaryTree(1), 1), True)
        self.assertEqual(lookup(getBinaryTree(2), 13), True)
    def test_isBST(self):
        self.assertEqual(isBST(getBinaryTree(0)), True)
        self.assertEqual(isBST(getBinaryTree(1)), True)
        self.assertEqual(isBST(getBinaryTree(2)), False)
        self.assertEqual(isBST(getBinaryTree(3)), True)
        self.assertEqual(isBST(getBinaryTree(4)), False)
        self.assertEqual(isBST(getBinaryTree(5)), True)
        self.assertEqual(isBST(getBinaryTree(6)), False)
    def test_hasPathSum(self):
        self.assertEqual(hasPathSum(getBinaryTree(2), 27), True)
        self.assertEqual(hasPathSum(getBinaryTree(2), 22), True)
        self.assertEqual(hasPathSum(getBinaryTree(2), 21), False)
        self.assertEqual(hasPathSum(getBinaryTree(6), 12), True)
if __name__ == '__main__':
    unittest.main()
    
