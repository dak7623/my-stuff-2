import collections

def bfs0(g, startnode, numnodes):
    """
    Perform a breadth-first search on g starting at node startnode and
    exploring up to numnodes unique nodes.

    Returns a tuple containing the distances from startnode to each
    node, the predecessors of each node in the BFS search tree, the
    labels of each edge in the BFS search tree, and the set of visited
    nodes.
    """
    dist = {startnode:0}
    pred = {startnode:None}
    label = {}
    visited = set([startnode])
    tempkey = 0
    counter = 0
    # First in first out (FIFO) # Stack
    queue = collections.deque([startnode]) # [0]
    # Fill in code
    # for i in range(len(g))
    # while queue is not empty AND len(visited) not equal numnodes
    #   get the first element from queue - currentNode
    #   do stuff, adding new nodes to the queue, caculate dist, pred, etc. with regards to the neighboring nodes
    #   move currentNode to visited
    # 
    while queue and len(visited) != numnodes: #TODO: because the order of visited is not pre-determined
        i = queue.popleft()
        for x in (g[i] - visited):   #thing is in graph set points
            # 4 not in pred, 4 not in dist
            if x not in pred:
                pred[x] = i #if in both don't add again
                label[(i,x)] = 't'
                dist[x] = dist[i]+1
                queue.append(x)
            else:
                #if (x,i) in label and label[(x,i)] == 't':
                    #label[(x,i)] = 't'
                #else:
                label[(i,x)] = 'c'
                    #if (x,i) in label and label[(x,i)] == 'c':
                        #del label[(i,x)]
        visited.add(i)

        # pred[key] = value
        '''
    for a in range(startnode+1, numnodes):
        tempkey = a
        counter = 0
        while tempkey != startnode:
            tempkey = pred[tempkey]
            counter+=1
        dist[a] = counter
        '''
    return (dist, pred, label, visited)


g1 = {
    0: set([1, 2]),
    1: set([3]),
    2: set([0, 3, 4]),
    3: set([1, 2, 5]),
    4: set([2, 5, 6]),
    5: set([3, 4]),
    6: set([4])
}

# visited: {0, 1, 2, 3, 4, 5, 6})
# pred: {0:none, 1:0, 2:0, 3:1, 4:2, 5:3, 6:4}
#print(bfs0(g1,0,7))
        
g4 = {0: set([1, 2, 3, 4, 5, 6, 7, 8]), 1: set([0, 2, 8]), 2: set([0, 1, 3]), 3:set([0, 2, 4]), 4:set([0, 3, 5]), 5:set([0, 4, 6]), 6: set([0, 5, 7]), 7:set([0, 6, 8]), 8:set([0, 1, 7])}
print(bfs0(g4,0,9))

g2 = {0: set([2, 3, 4, 1]), 1: set([0, 2, 5]), 2: set([1, 0, 5]), 3: set([0, 6, 7]),
4: set([0, 7]), 5: set([1, 2, 8]), 6: set([3, 9, 8]), 7: set([3, 4, 9]), 8: set([5, 6, 10]),
9: set([6, 7, 10]), 10: set([8, 9])}
print(bfs0(g2,0,11))