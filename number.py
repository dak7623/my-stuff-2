import unittest
'''
input: string of integers (e.g. "234")
output: list of possible string combinations (e.g. ["ADG", "ADH", etc..])

2 -> 'A', 'B', 'C'
...
7 -> 'P', 'Q', 'R', 'S'
opt 1 - {'A':2, 'B':2}
opt 2 - {2: 'ABC'} OR {2:['A', 'B','C']}
opt 3 - ['0', '1', 'ABC'] OR [['0'], ['1'], ['A', 'B', 'C']]

concatenate strings: 'A'+'D', ''.join(['A', 'D']) -> 'AD'
switch(num_digits)
 if 2 digits:
     for loop
       for loop 
 if 3 digit:
     for loop
        for loop
    
Iterative
digit_count = 0
while digit_count != len(num):
    for letter in current_digit_char_set
        partial_string += letter
    partial_string update next digit1a
    digit_count+=1

for i in range(x):
    for j in range(y):
        for k in range(z):
            str = mapping[i]+mapping[j]+mapping[k] #"mapping[i] ->'A' in 'ABC'

['ABC'] ['DEF']
'223' -> ['ABC'][0] ['ABC'][1] ['DEF'][0] -> 'ABD'
digit_pos = [0, 0, 0]
Recursive
'''
def numtoletter(num):
    numchars = {'0':'0', '1':'1', '2':'ABC', '3':'DEF', '4':'GHI', '5':'JKL', '6':'MNO', '7':'PQRS', '8':'TUV', '9':'WXYZ'}
    # numchars = ['0', '1', 'ABC', 'DEF', 'GHI', 'JKL', 'MNO', 'PQRS', 'TUV', 'WXYZ']
    return list(map(lambda x: list(numchars[x]), num))
'''[['A', 'B', 'C'], ['A', 'B', 'C'], ['D', 'E', 'F']]'''
def updateDigitpos(digit_pos, letterlist):
    digit_pos[-1]+= 1
    for p in range(1,len(digit_pos)):
        if digit_pos[-p] == len(letterlist[-p]):
            digit_pos[-p] = 0
            digit_pos[-p-1] += 1
def final(num):
    final = []
    digit_pos = []
    if len(num) > 0:
        total = 1
    else:
        total = 0
    letterlist = numtoletter(num)
    for i in range(len(num)):
        digit_pos.append(0)
        total = total*len(letterlist[i])
    
    for c in range(total):
        stringy = ""
        # forming the string
        for w in range(len(letterlist)):
            stringy += letterlist[w][digit_pos[w]]
        
        # update the digit position
        final.append(stringy)
        updateDigitpos(digit_pos, letterlist)
    return final 
# print(final("192835"))

 

class NumToLetter_Test(unittest.TestCase):
    '''
    def test_final(self):
        self.assertEqual(final(numtoletter('23')), ["AD", "AE", "AF", "BD", "BE", "BF", "CD", "CE", "CF"])
        self.assertEqual(final(numtoletter('234')), ["ADG", "ADH", "ADI", "AEG", "AEH", "AEI", "AFG", "AFH", 
                                            "AFI", "BDG", "BDH", "BDI", "BEG", "BEH", "BEI", "BFG", "BFH", "BFI", 
                                            "CDG", "CDH", "CDI", "CEG", "CEH", "CEI", "CFG", "CFH", "CFI"])
    '''
    def test_fin0(self):
        self.assertEqual(final(""), [])
    def test_fin1(self):
        self.assertEqual(final("1"), ["1"])
    def test_fin2(self):
        self.assertEqual(final("12"), ["1A", "1B", "1C"])
    def test_fin3(self):
        self.assertEqual(final("2"), ["A", "B", "C"])
    def test_fin4(self):
        self.assertEqual(final("22"), ["AA", "AB", "AC", "BA", "BB", "BC", "CA", "CB", "CC"])
    def test_fin5(self):
        self.assertEqual(final("23"), ["AD", "AE", "AF", "BD", "BE", "BF", "CD", "CE", "CF"])
    def test_fin6(self):
        self.assertEqual(final("234"), ["ADG", "ADH", "ADI", "AEG", "AEH", "AEI", "AFG", "AFH","AFI", "BDG", "BDH", "BDI", "BEG", "BEH", "BEI", "BFG", "BFH", "BFI", "CDG", "CDH", "CDI", "CEG", "CEH", "CEI", "CFG", "CFH", "CFI"])
    def test_fin7(self):
        self.assertEqual(final("17"), ['1P', '1Q', '1R', '1S'])
                        
if __name__ == '__main__':
    unittest.main()