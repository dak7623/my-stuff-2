import unittest

'''
Lexicographical order is known as alphabetical order when dealing with strings. A string is greater than another string if it comes later in a lexicographically sorted list.

Given a word, create a new word by swapping some or all of its characters. This new word must meet two criteria:
    It must be greater than the original word
    It must be the smallest word that meets the first condition

Example:
    w = 'abcd'
The next largest word is 'abdc'.


Complete the function isGreater below to create and return the new string meeting the criteria. If it is not possible, return no answer.

Function Description
    Complete the isGreater function in the editor below.
    isGreater has the following parameter(s):
        - string w: a word
    Returns 
        - string: the smallest lexicographically higher string possible or no answer

Example 1:
w = 'ab'
return 'ba'

Example 2:
w = 'nefg'
return 'negf'

Example 3:
w = 'dhck'
return 'dhkc'

Example 4:
w = 'dkhc'
return 'hcdk'

fedcba
'''

def isGreater(w):
    w = list(w)  
    for i in range(len(w)-1, 1, -1):
        if(w[i] > w[i-1]):
            return ''.join(w[:(i-1)] + swap(w[(i-1):]))
    temp = swap(w[0:])
    if temp == None:
       return None
    return ''.join(temp)
def swap(w):
    larger = []
    for i in w:
        if i > w[0]:
            larger.append(i)
    if(len(larger) == 0):
        return None
    temp = min(larger)
    del w[w.index(temp)]
    w.sort()
    return [temp] + w
        
        
            


class IsGreater_Test(unittest.TestCase):
    # Base tests
    def test_0(self):
        self.assertEqual(isGreater('ab'), 'ba')
        self.assertEqual(isGreater('nefg'), 'negf')
        self.assertEqual(isGreater('dhck'), 'dhkc')
        self.assertEqual(isGreater('dkhc'), 'hcdk')
        self.assertEqual(isGreater('khdc'), None)
        self.assertEqual(isGreater('zpeqesumvo'), 'zpeqesuomv')
        self.assertEqual(isGreater('flionvjjob'), 'flionvjobj')
        self.assertEqual(isGreater('exjkxccvaqikgrvnxkkf'), 'exjkxccvaqikgrvxfkkn')
        self.assertEqual(isGreater('zviofydkkwtbwafsykog'), 'zviofydkkwtbwafsyogk')

if __name__ == '__main__':
    unittest.main()