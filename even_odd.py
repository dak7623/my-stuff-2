import unittest
#http://www.usaco.org/index.php?page=viewproblem2&cpid=1084
def even_odd(val):
    even = [i%2 for i in val].count(0)
    odd = len(val)-even
    overlap = min(even, odd)
    diff = even-odd
    return 2*overlap+1 if diff>0 else 2*overlap+odd_grp(abs(diff))

def odd_grp(val):
    cases = {0:0, 1:-1, 2:1, 3:2, 4:1}
    if val in cases.keys():
        return cases[val]
    return 2+odd_grp(val-3)

# class Test_even_odd(unittest.TestCase):
#     def test_even_odd(self):
#         self.assertEqual(even_odd([11, 2, 17, 13, 1, 15, 3]), 5)
#         self.assertEqual(even_odd([1, 3, 5, 7, 9, 11, 13]), 3)

# if __name__ == '__main__':
#     unittest.main()

num = int(input())
int_array = [int(i) for i in input().split()]
print(even_odd(int_array))
